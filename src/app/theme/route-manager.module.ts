import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './default/pages/homepage/homepage.component';
import { DefaultModule } from './default/default.module';
import { API } from '../providers/webservice/api.service';
import { ScriptLoaderService } from '../providers/application/utils/script-loader.service';
import { Helper } from '../providers/application/utils/helper.service';
import { CelebritiesComponent } from './default/pages/celebrities/celebrities.component';
import { CelebrityProfileComponent } from './default/pages/celebrities/profile/profile.component';
import { EventsModule } from '../events/events.module';
import { VendorsModule } from '../vendors/vendors.module';
import { SearchComponent } from './default/pages/search/search.component';
import { InsComponent } from './default/pages/ins/ins.component';
import { NotFoundComponent } from './default/pages/not-found-component/not-found-component.component';
//import { SiteMaintenanceComponent } from './default/pages/site-maintenance/site-maintenance-component.component';
// import { LocalStorage } from '../providers/application/storage/local-storage.service';
// import { Session } from '../providers/application/session/session.service';

const putUnderMaintenance =false;

let themeRoute: Routes =[

  {
    path: '',
    component: HomepageComponent,
    data: { lang: 'en' }
  },
  {
    path: 'hi',
    component: HomepageComponent,
    data: { lang: 'hi' }
  },
  {
    path: 'celebrity',
    component: CelebritiesComponent,
    data: { lang: 'en' }
  },
  {
    path: 'hi/celebrity',
    component: CelebritiesComponent,
    data: { lang: 'hi' }
  },
  {
    path: 'celebrity/:name',
    component: CelebrityProfileComponent,
    data: { lang: 'en' }
  },
  {
    path: 'hi/celebrity/:name',
    component: CelebrityProfileComponent,
    data: { lang: 'hi' }
  },
  {
    path: 'search/:keyword',
    component: SearchComponent,
    data: { lang: 'en', type: 'search' }
  },
  {
    path: 'hi/search/:keyword',
    component: SearchComponent,
    data: { lang: 'hi', type: 'search' }
  },
  {
    path: 'search',
    component: SearchComponent,
    data: { lang: 'en', type: 'search' }
  },
  {
    path: 'hi/search',
    component: SearchComponent,
    data: { lang: 'hi', type: 'search' }
  },
  {
    path: 'tags',
    component: SearchComponent,
    data: { lang: 'en', type: 'tags' }
  },
  {
    path: 'hi/tags',
    component: SearchComponent,
    data: { lang: 'hi', type: 'tags' }
  },
  {
    path: 'tags/:keyword',
    component: SearchComponent,
    data: { lang: 'en', type: 'tags' }
  },
  {
    path: 'hi/tags/:keyword',
    component: SearchComponent,
    data: { lang: 'hi', type: 'tags' }
  },
  // commented as per brian on 26 aug, 2019 swipe routes up and down of INS for url confliction
  {
    path: 'agencies/:title/:id',
    component: InsComponent,
    data: { lang: 'en' }
  },
  {
    path: 'hi/agencies/:title/:id',
    component: InsComponent,
    data: { lang: 'hi' }
  },
  {
    path: 'agencies/:agency',
    // component: InsComponent,
    redirectTo: 'not-found',
    data: { lang: 'en' }
  }, {
    path: 'hi/agencies/:agency',
    // component: InsComponent,
    redirectTo: 'not-found',
    data: { lang: 'hi' }
  },
  {
    path: 'not-found',
    component: NotFoundComponent,
    data: { lang: 'en' }
  },
  {
    path: 'footers',
    loadChildren: './default/pages/footer/footer.module#FooterModule'
  },
  {
    path: 'hi/:parent',
    loadChildren: './default/pages/categories/categories.module#CategoriesModule',
    data: { lang: 'hi' }
  },
  {
    path: 'author',
    loadChildren: './default/pages/authors/authors.module#AuthorsModule',
    data: { lang: 'en' }
  },
  {
    path: ':parent',
    loadChildren: './default/pages/categories/categories.module#CategoriesModule',
    data: { lang: 'en' }

  },
  {
    path: '**',
    redirectTo: 'not-found'
  },
];

// if(putUnderMaintenance){
//   themeRoute=[
//      {
//     path: '*',
//     component: SiteMaintenanceComponent,
//     data: { lang: 'en' }
//   },
//   {
//     path: '**',
//     component: SiteMaintenanceComponent,
//     data: { lang: 'en' }
//   } ]
// }else{

// themeRoute=[]

// }



@NgModule({
  imports: [
    CommonModule,
    DefaultModule,
    RouterModule.forRoot(themeRoute, {preloadingStrategy: PreloadAllModules}),
    VendorsModule
  ],
  exports: [RouterModule, DefaultModule],
  declarations: [HomepageComponent],
  providers: [API, ScriptLoaderService, Helper, EventsModule],

})
export class RouteManagerModule { }
