import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-article-container',
  templateUrl: './article.fragment.html'
})
export class ArticleFragmentComponent implements OnInit {
  @Input('content') article: any;
  constructor() { }

  ngOnInit() {
  }

}
