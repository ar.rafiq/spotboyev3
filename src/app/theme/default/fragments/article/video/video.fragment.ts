import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Helper } from '../../../../../providers/application/utils/helper.service';

@Component({
  selector: 'app-video-container',
  templateUrl: './video.fragment.html'
})
export class VideoFragmentComponent implements OnChanges {
  @Input() content: any;
  videoUrl: any;
  constructor(private safe: DomSanitizer, private helper: Helper) { }

  ngOnChanges() {

    if (this.content.youtubeId) {
      this.videoUrl = this.safe.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.content.youtubeId);
    }
  }


}
