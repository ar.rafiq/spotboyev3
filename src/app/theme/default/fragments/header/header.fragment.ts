import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { MenuService } from '../../../../events/menu/menu.service';
import { Router, NavigationEnd } from '@angular/router';
import { API } from '../../../../providers/webservice/api.service';
import { Helper } from '../../../../providers/application/utils/helper.service';
import { InterCommunicateService, CommunicationPayloadModel } from '../../../../events/inter-communicate/inter-communicate.service';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.fragment.html'
})
export class HeaderComponent implements OnInit {
  menus: Response.MainMenu;
  isArticlePage: Boolean = false;
  pageTitle: String = 'Spotboye';
  currentLang: String = 'en';
  constructor(private menu: MenuService, private route: Router, private api: API,
    private helper: Helper, private communicate: InterCommunicateService, @Inject(PLATFORM_ID) private pID) { }

  ngOnInit() {
    this.communicate.listen$.subscribe((_d: CommunicationPayloadModel) => {
      if (_d && _d.type === InterCommunicateService.LOGO_ALT) {
        this.pageTitle = _d.payload;
      }
    });
    this.route.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isArticlePage = (event.url.split('/').length > 4);
      }
    });
    this.currentLang = this.api.getLang();
    this.menu.listen().subscribe(menu => {
      this.menus = menu;

    });
  }


  getSubmenuArticle(event, p) {
    p.skip = 0;
    p.limit = 3;
    event.target.nextSibling.innerHTML = '<li class="menu-item">Please wait..</li>';
    this.api.category(p).subscribe((r: Response.BaseResponse) => {
      if (r.success === true) {
        event.target.nextSibling.innerHTML = r.data.data.map(item => {
          // tslint:disable-next-line:max-line-length
          return `<li class="menu-item"><a href="${this.helper.stripURL(item.shareURL)}"> <img src="${item.thumbnail}"><span>${item.title}</span></a></li>`;
        }).join('');
      }
    });
  }

  public onIconClick() {
    this.currentLang = 'en';
  }

}
