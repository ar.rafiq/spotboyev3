import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../../../events/menu/menu.service';
import { API } from '../../../../providers/webservice/api.service';
import * as $ from 'jquery';
@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {
  menus: Response.MainMenu;
  currentLang: String = 'en';
  constructor(private menuService: MenuService, private api: API) {
  }

  ngOnInit() {

    this.currentLang = this.api.getLang();
    this.menuService.listen().subscribe(menus => {
      this.menus = menus;
    });
  }
  closeDrawer() {
    $('#mvp-fly-wrap').toggleClass('mvp-fly-menu');
    $('#mvp-fly-wrap').toggleClass('mvp-fly-shadow');
    $('.mvp-fly-but-wrap').toggleClass('mvp-fly-open');
    $('.mvp-fly-fade').toggleClass('mvp-fly-fade-trans');
  }

}
