import { Component, OnInit } from '@angular/core';
import { API } from '../../../../../providers/webservice/api.service';


@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.scss']
})
export class SitemapComponent implements OnInit {
  menus: any = [];
  constructor(private api: API) { }

  ngOnInit() {
    this.api.menu().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        const lang = this.api.getLang();
        this.menus = res.data.map(_i => {
          _i.langURL = (lang === 'en') ? '' : '/hi';
          return _i;
        });
        console.log('menus recvd',this.menus);
      }
    });
  }

}
