import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { SitemapComponent } from './sitemap/sitemap.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { Routes, Router, RouterModule } from '@angular/router';
import { EventsModule } from '../../../../events/events.module';

const footersRoute: Routes = [
  {
    path: 'about-us',
    component: AboutUsComponent
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  {
    path: 'disclaimer',
    component: DisclaimerComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'terms-conditions',
    component: TermsConditionsComponent
  }, {
    path: 'sitemap',
    component: SitemapComponent
  },
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(footersRoute),
    EventsModule
  ],
  declarations: [DisclaimerComponent,
    PrivacyPolicyComponent, ContactUsComponent, AboutUsComponent, TermsConditionsComponent,
    SitemapComponent],
  exports: [EventsModule]
})
export class FooterModule { }
