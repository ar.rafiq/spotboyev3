import { Component, OnInit} from '@angular/core';
import { Title } from '@angular/platform-browser';

import { ContentOnloadService } from '../../../../events/content-onload/content-onload.service';
@Component({
  selector: 'app-site-maintenance-component',
  templateUrl: './site-maintenance-component.component.html',
  styleUrls: ['./site-maintenance-component.component.scss']
})
export class SiteMaintenanceComponent implements OnInit {

  constructor(private titleService: Title, private contentOnLoad: ContentOnloadService) {
    this.titleService.setTitle('Spotboye - Site is under maintenance');
  }

  ngOnInit() {
    this.contentOnLoad.loaded(true);

  }

}
