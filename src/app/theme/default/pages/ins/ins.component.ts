import { Component, OnInit } from '@angular/core';
import { API } from '../../../../providers/webservice/api.service';
import { Helper } from '../../../../providers/application/utils/helper.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ContentOnloadService } from '../../../../events/content-onload/content-onload.service';

@Component({
  selector: 'app-ins',
  templateUrl: './ins.component.html',
  styleUrls: ['./ins.component.scss']
})
export class InsComponent implements OnInit {
  list: Array<any> = [];
  content: any = {};
  loaded: Boolean = false;
  isListing: Boolean = true;
  lang: String = '';
  constructor(private api: API, private helper: Helper, private route: ActivatedRoute, private sanitizer: DomSanitizer,
    private onload: ContentOnloadService) {
  }

  ngOnInit() {
    this.route.data.subscribe(d => {
      this.lang = d.lang + '/';
      this.route.params.subscribe(params => {
        if (params.id) {
          this.api.http.get(`https://apiv3.spotboye.com/web/${this.lang}agencies/ins/` + params.id).subscribe((res: any) => {

            if (res.success === true) {
              this.content = res.data.data;
              this.content.body = (res.data.data.body) ? this.sanitizer.bypassSecurityTrustHtml(res.data.data.body) : '';
              this.loaded = true;
              this.isListing = false;
              this.helper.setTitle(res.data.metaTitle);
              this.helper.setCanonical(this.content.shareURL);
              this.helper.updateMeta({
                title: res.data.metaTitle,
                description: res.data.metaDescription,
                keywords: res.data.metaKeywords,
                url: this.content.shareURL
              });
            }
            this.onload.loaded(true);

          });
        } else {
          this.api.http.get(`https://apiv3.spotboye.com/web/${this.lang}agencies/ins`).subscribe((res: any) => {
            if (res.success === true) {
              this.list = res.data;
              this.loaded = true;
              this.isListing = true;
            }

          });
          this.onload.loaded(true);

        }
      });
    });



  }

}
