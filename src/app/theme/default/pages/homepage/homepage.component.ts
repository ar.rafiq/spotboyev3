import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { API } from '../../../../providers/webservice/api.service';
import { Helper } from '../../../../providers/application/utils/helper.service';
import { ContentOnloadService } from '../../../../events/content-onload/content-onload.service';
import { InterCommunicateService } from '../../../../events/inter-communicate/inter-communicate.service';
import { GAService } from '../../../../vendors/google-analytics/ga.service';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  dashboard: Array<Response.Section> = [];
  loaded: Boolean = false;
  currentLang: String = 'en';
  window: any = {};
  constructor(private api: API, private helper: Helper, private onload: ContentOnloadService,
    private communicate: InterCommunicateService, @Inject(PLATFORM_ID) private platform_id, private route: ActivatedRoute,
    private ga: GAService) {
    if (isPlatformBrowser(platform_id)) {
      this.window = window;
    }
    this.route.data.subscribe(d => {
      this.currentLang = d.lang;
    });

  }
  tabs: any = [];

  ngOnInit() {


    this.api.setLang(this.currentLang);
    this.api.dashboard().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        this.loaded = true;
        this.dashboard = res.data.data;
        this.helper.setTitle(res.data.metaTitle);
        this.helper.updateMeta({
          title: res.data.metaTitle,
          description: res.data.metaDescription,
          keywords: res.data.metaKeywords.join(),
          imageURL: res.data.image,
          url: 'https://www.spotboye.com' + this.api.getLangURL(),
          locale: ((this.currentLang === 'hi') ? 'hi_IN' : 'en_US')
        });
        if (this.currentLang === 'hi') {
          this.helper.setCanonical('https://www.spotboye.com/hi');
        } else {
          this.helper.setCanonical('https://www.spotboye.com/');
        }
        // Add ALT to Spotboye LOGO
        this.communicate.send({ type: InterCommunicateService.LOGO_ALT, payload: res.data.metaTitle });
      }
      this.onload.loaded(true);
    });
    this.api.tabs().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        res.data.forEach(tab => {
          this.tabs.push({ name: tab.name, id: `tab-${tab.name}-1`, data: tab.data });
        });
      }

    });

  }

  public vidPlay() {
    this.ga.gaVideoEvent('Videos', 'Play', 'Video Start', 'Spotboye Video');
  }

  public vidProgress() {
    this.ga.gaVideoEvent('Videos', 'Buffer', 'Video Buffering', 'Spotboye Video');
  }

  public vidPause() {
    this.ga.gaVideoEvent('Videos', 'Pause', 'Video Pause', 'Spotboye Video');
  }

  public vidPlaying() {
    this.ga.gaVideoEvent('Videos', 'Playing', 'Video Playing', 'Spotboye Video');
  }

  public vidSeeked() {
    this.ga.gaVideoEvent('Videos', 'Seeked', 'Video Seeked', 'Spotboye Video');
  }

  public vidEnded() {
    this.ga.gaVideoEvent('Videos', 'End', 'Video Ended', 'Spotboye Video');
  }

}
