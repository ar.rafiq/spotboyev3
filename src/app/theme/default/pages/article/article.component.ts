import { Component, AfterViewInit, Inject, ElementRef, PLATFORM_ID, ChangeDetectionStrategy, Renderer2 } from '@angular/core';
import { API } from '../../../../providers/webservice/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Helper } from '../../../../providers/application/utils/helper.service';
import { ScriptLoaderService } from '../../../../providers/application/utils/script-loader.service';
import { ContentOnloadService } from '../../../../events/content-onload/content-onload.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class ArticleComponent implements AfterViewInit {
  article: Response.Article;
  loaded: Boolean = false;
  tabs: Array<Application.Tabs> = [];
  twitterLoaded: Boolean = false;
  window: any = {};
  langURL: String = '';
  gid: any = '1';
  deviceType = 'mobile';
  showNowInHindi = false;

  constructor(@Inject(DOCUMENT) private doc, private elem: ElementRef, private api: API, private route: ActivatedRoute,
    private router: Router, private helper: Helper, @Inject(PLATFORM_ID) private p_id,
    private scriptLoader: ScriptLoaderService, private onload: ContentOnloadService,
    private renderer: Renderer2) {
    if (isPlatformBrowser(p_id)) {
      this.window = window;
    }

  }
  closeHindiTab() {
    console.log('close hindi');
    return this.showNowInHindi = false;
  }
  getRandom() {
    return Math.floor(Math.random() * 3 + 1);
  }
  ngAfterViewInit() {

    this.route.params.subscribe(param => {

      this.api.article(param.articleID).subscribe((res: any) => {
        this.onload.loaded(true); // Hide the shell box UI
        this.loaded = true;
        if (res.success === true) {
          this.gid = this.getRandom();
          this.article = res.data;
          this.article.moreVideos = [];
          this.article.locale = (res.data.lang && res.data.lang === 'en') ? '' : '/hi';
          if (res.data.lang === 'en') {
            this.showNowInHindi = true;
          }
          this.createBreadcrumbSchema();
          this.createArticleSchema();
          this.createAmpLink();
          this.updateMeta();
          if (this.article.type === 'Video') {
            this.getMoreVideos();
            this.createVideoSchema();
          }

          if (isPlatformBrowser(this.p_id)) {
            // commented as per sandeep 10/02/2020 
            /*setTimeout(() => {
              this.insertMiddleContent();
            }, 0);*/
            if (this.window.screen.width >= 700) {
              this.deviceType = 'desktop';
            } else {
              this.deviceType = 'mobile';

            }
          }

          if (this.window.twttr) {

            setTimeout(() => {
              this.window.twttr.widgets.load();
            }, 0);

          } else {
            // Listen to loaded script
            this.scriptLoader.onLoad$.subscribe(url => {
              switch (url) {
                case 'https://platform.twitter.com/widgets.js':
                  this.window.twttr.widgets.load();
                  break;
              }
            });
          }
        } else {
          this.router.navigate(['not-found']);
        }


      });
    });


    this.api.tabs().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        res.data.forEach(tab => {
          this.tabs.push({ name: tab.name, id: `tab-${tab.name}-1`, data: tab.data });
        });
      }

    });

  }


  insertMiddleContent() {
    // Insert mid article ads
    let _adtag = '';
    // let floatingAdtag = '';
    if (this.window.screen.width >= 700) {
      _adtag = `googletag.defineSlot('/97172535/Spotboye_AP_AboveIF_Desktop', [728, 90],'div-gpt-ad-1540908935129-0');`;
      // floating ad variable of novorell for desktop
      // floatingAdtag = `<script async id="aniviewJS800328045" src="https://play.aniview.com/5b59760c073ef46a2e6b8f13/5cc876dd073ef434ba5e0192/spotboye_spotboye.com_Desktop_Floating_Rev70_3004.Js"></script>`;
    } else {
      _adtag = `googletag.defineSlot('/97172535/Spotboye_AP_AboveIF_Mobile', [320, 100],'div-gpt-ad-1540908935129-0');`;
      // floating ad variable of novorell for mobile
      // floatingAdtag = `<script async id="aniviewJS429957275" src="https://play.aniview.com/5b59760c073ef46a2e6b8f13/5ccaf12b073ef47f796afaf9/spotboye_spotboye.com_Mobile_Incontent_Rev70_3004.Js"></script>`;
    }

    const _ad = `<div id='InRead'>
    <script>
    googletag.cmd.push(function() {
      var slot=${_adtag};
      if(slot){
        slot.addService(googletag.pubads());
        googletag.pubads();
        googletag.enableServices();
        googletag.display('div-gpt-ad-1540908935129-0');
      }else{
        googletag.pubads().refresh('div-gpt-ad-1540908935129-0');
      }
    });
  </script>

  <div id='div-gpt-ad-1540908935129-0'>

  </div></div>`;
  /* const showtAd = `<script type="text/javascript" src="https://dujy1obvgbkk2.cloudfront.net/widgets/index.js"
  data-showtbox-include="" data-widget-type="events_widget" data-event-id="3320" data-height="600px"
  data-width="300px" data-fullscreen-link="https://www.showt.com" data-app-download='false' data-full-footer='false'
  data-fixed-header='true' data-iscroll-bar='false' data-lang="en" data-edition="in_en" data-social-share="true"
  data-widget-source="spotboye"></script>`; */
    if ($('#mvp-content-main').find('p').contents().filter('br:eq(3)').length > 0) {
      $(_ad).insertBefore($('#mvp-content-main').find('.MsoNoSpacing').contents().filter('br:eq(3)'));
    } else {

      if ($('#mvp-content-main').find('p:eq(2)').length > 0) {

        $(_ad).insertBefore($('#mvp-content-main').find('p:eq(2)'));
      } else {

        $(_ad).insertBefore($('#mvp-content-main').find('div:eq(2)'));
      }
    }
    // $('#showtAdid').append(showtAd);
  }

  createBreadcrumbSchema() {
    let data: any = {};
    if (this.article.locale === '') {
      // English
      data = {
        '@context': 'http://schema.org',
        '@type': 'BreadcrumbList',
        itemListElement: [
          {
            '@type': 'ListItem',
            position: 1,
            item: {
              '@id': `https://www.spotboye.com`,
              name: 'Homepage'
            }
          },
          {
            '@type': 'ListItem',
            position: 2,
            item: {
              '@id': `https://www.spotboye.com/${this.article.mainSection.parentSlug}`,
              name: `${this.article.mainSection.parentSection}`
            }
          },
          {
            '@type': 'ListItem',
            position: 3,
            item: {
              '@id': `https://www.spotboye.com/${this.article.mainSection.parentSlug}/${this.article.mainSection.slug}`,
              name: `${this.article.mainSection.title}`
            }
          }
        ]
      };
    } else {
      // Hindi
      data = {
        '@context': 'http://schema.org',
        '@type': 'BreadcrumbList',
        itemListElement: [
          {
            '@type': 'ListItem',
            position: 1,
            item: {
              '@id': `https://www.spotboye.com`,
              name: 'Homepage'
            }
          },
          {
            '@type': 'ListItem',
            position: 2,
            item: {
              '@id': `https://www.spotboye.com/hi`,
              name: 'Hindi'
            }
          },
          {
            '@type': 'ListItem',
            position: 3,
            item: {
              '@id': `https://www.spotboye.com/hi/${this.article.mainSection.parentSlug}`,
              name: `${this.article.mainSection.parentSection}`
            }
          },
          {
            '@type': 'ListItem',
            position: 4,
            item: {
              '@id': `https://www.spotboye.com/hi/${this.article.mainSection.parentSlug}/${this.article.mainSection.slug}`,
              name: `${this.article.mainSection.title}`
            }
          }
        ]
      };
    }


    const schemaEl = this.renderer.createElement('script');
    schemaEl.type = `application/ld+json`;
    schemaEl.text = `${JSON.stringify(data)}`;

    setTimeout(() => {
      // console.log(this.elem.nativeElement.getElementsByTagName('div')[0], 'DIV');
      this.renderer.appendChild(this.elem.nativeElement, schemaEl);
    }, 0);
  }

  createArticleSchema() {
    const data: any = {
      '@context': `http://schema.org`,
      '@type': `NewsArticle`,
      headline: `${this.article.title}`,
      image: {
        '@type': `ImageObject`,
        url: `${this.article.thumbnail}`,
        height: 500,
        width: 800
      },
      datePublished: `${this.article.publishedAt}`,
      dateModified: `${this.article.modifiedAt}`,
      mainEntityOfPage: `${this.article.shareURL}`,
      author: {
        '@type': `Person`,
        name: `${this.article.authorName}`
      },
      publisher: {
        '@type': `Organization`,
        name: `Spotboye`,
        logo: {
          '@type': `ImageObject`,
          url: `http://www.spotboye.com/images/logo.png`,
          width: 180,
          height: 40
        }
      },
      description: `${this.article.shortBody}`
    };
    if (this.article.publishedAt !== this.article.modifiedAt) {
      data.dateModified = `${this.article.modifiedAt}`;
    }
    const schemaEl = this.renderer.createElement('script');
    schemaEl.type = `application/ld+json`;
    schemaEl.text = `${JSON.stringify(data)}`;
    setTimeout(() => {
      // console.log(this.elem.nativeElement.getElementsByTagName('div')[0], 'DIV');
      this.renderer.appendChild(this.elem.nativeElement, schemaEl);
    }, 0);
  }
  createVideoSchema() {

    const data = {
      '@context': `http://schema.org`,
      '@type': `VideoObject`,
      name: `${this.article.title}`,
      description: `${this.article.shortBody}`,
      thumbnailUrl: `${this.article.thumbnail}`,
      uploadDate: `${this.article.publishedAt}`,
      embedUrl: `https://www.youtube.com/embed/${this.article.youtubeId}`,
      publisher: {
        '@type': `Organization`,
        name: `Spotboye`,
        logo: {
          '@type': `ImageObject`,
          url: `http://www.spotboye.com/images/logo.png`,
          width: 180,
          height: 40
        }
      },
    };
    const schemaEl = this.renderer.createElement('script');
    schemaEl.type = `application/ld+json`;
    schemaEl.text = `${JSON.stringify(data)}`;
    setTimeout(() => {
      // console.log(this.elem.nativeElement.getElementsByTagName('div')[0], 'DIV');
      this.renderer.appendChild(this.elem.nativeElement, schemaEl);
    }, 0);
  }
  createAmpLink() {
    /*
    const url = this.article.shareURL.replace('/hi', '').replace('.com', '.com/amp');
    const ampEl = this.doc.createElement('link');
    ampEl.rel = 'amphtml';
    ampEl.href = url;
    setTimeout(() => {

      if (this.elem.nativeElement.getElementsByTagName('div')[0]) {
        this.elem.nativeElement.getElementsByTagName('div')[0].appendChild(ampEl);
      }
    }, 0);
    */

    const url = this.article.shareURL.replace('/hi/', '/').replace('.com', '.com/amp');
    let link: HTMLLinkElement = this.doc.createElement('link');
    link.setAttribute('rel', 'amphtml');
    link.setAttribute('href', url);
    this.doc.head.appendChild(link);
  }
  updateMeta() {
    this.helper.setTitle(this.article.metaTitle);
    this.helper.setCanonical(this.article.shareURL);
    this.helper.updateMeta({
      title: this.article.metaTitle,
      description: this.article.metaDescription,
      keywords: this.article.metaKeywords.join(),
      imageURL: this.article.thumbnail,
      url: this.article.shareURL,
      locale: ((this.article.locale === '/hi') ? 'hi_IN' : 'en_US')
    });
  }

  getMoreVideos() {
    const payload = {
      type: 'related',
      searchby: ['celebrity', 'tags'],
      search: {
        celebrity: this.article.celebritytags.map(i => i._id),
        tags: this.article.tags
      },
      limit: 4
    };
    this.api.more({ type: 'video', id: this.article._id }, payload).subscribe((result: Response.BaseResponse) => {
      if (result.success === true) {
        this.article.moreVideos = result.data;

      }
    });
  }
}
