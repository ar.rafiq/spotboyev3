import { Component, OnInit, Inject, PLATFORM_ID, ElementRef, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { API } from '../../../../../providers/webservice/api.service';
import { Helper } from '../../../../../providers/application/utils/helper.service';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ContentOnloadService } from '../../../../../events/content-onload/content-onload.service';
import { InterCommunicateService } from '../../../../../events/inter-communicate/inter-communicate.service';
import { GAService } from '../../../../../vendors/google-analytics/ga.service';
@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit, OnDestroy {

  categories: any = [];
  loaded: Boolean = false;
  tabs: Array<Application.Tabs> = [];
  parentCategory: any = { name: '', slug: '' };
  childCategory: any = { name: '', slug: '' };
  result: any;
  page: any = 1;
  limit: any = 30;
  skip: Number = 0;
  navigationListener: Subscription;
  firstLoad: Boolean = true;
  langURL: String = '';
  constructor(private api: API, private route: ActivatedRoute, private helper: Helper, @Inject(PLATFORM_ID) private platform_id,
    @Inject(DOCUMENT) private doc, private elem: ElementRef, private router: Router, private onload: ContentOnloadService,
    private communicate: InterCommunicateService, private ga: GAService) { }

  ngOnInit() {
    this.route.data.subscribe(d => {
      this.api.setLang(d.lang);
      this.langURL = (d.lang === 'en') ? '' : d.lang + '/';
    });
    this.route.params.subscribe(param => {
      this.parentCategory.slug = param.parent;
      this.childCategory.slug = param.child;
      this.page = (param.page) ? Number.parseInt(param.page) : 1;
      this.loadArticles();
    });

    this.api.tabs().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        res.data.forEach(tab => {
          this.tabs.push({ name: tab.name, id: `tab-${tab.name}-1`, data: tab.data });
        });
      }

    });

    this.navigationListener = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      this.categories = [];
    });
  }
  ngOnDestroy() {
    if (this.navigationListener) {
      this.navigationListener.unsubscribe();
    }
  }

  createWebpageSchema() {
    const data = {
      '@context': 'http://schema.org',
      '@type': 'WebPage',
      'name': this.childCategory.name,
      'description': this.result.metaDescription,
      'url': `/${this.parentCategory.slug}/${this.childCategory.slug}`,
      'publisher': {
        '@type': 'Organization',
        'name': 'SpotboyE',
        'url': 'https://www.spotboye.com/',
        'logo': {
          '@type': 'ImageObject',
          'contentUrl': '/assets/images/flat_logo.png'
        }
      }
    };

    const schemaEl = this.doc.createElement('script');
    schemaEl.type = `application/ld+json`;
    schemaEl.text = `${JSON.stringify(data)}`;
    setTimeout(() => {
      // console.log(this.elem.nativeElement.getElementsByTagName('div')[0], 'DIV');
      this.elem.nativeElement.getElementsByTagName('div')[0].appendChild(schemaEl);
    }, 0);
  }


  isBrowser() {
    return isPlatformBrowser(this.platform_id);
  }
  loadMoreLink(prev) {
    return (prev) ? ((this.page > 1) ? `/${this.parentCategory.slug}/${this.childCategory.slug}/page/${this.page - 1}` : '') :
      `/${this.parentCategory.slug}/${this.childCategory.slug}/page/${this.page + 1}`;
  }
  loadMore() {
    this.firstLoad = false;
    this.page++;
    this.loadArticles();
  }
  loadArticles() {
    // for setting canonical url when secondary category hitted without page (this.page = 1)
    let canUrl = `https://www.spotboye.com${this.api.getLangURL()}/${this.parentCategory.slug}/${this.childCategory.slug}/page/${this.page}`;
    if (this.page === 1 || this.page === 0) {
      canUrl = `https://www.spotboye.com${this.api.getLangURL()}/${this.parentCategory.slug}/${this.childCategory.slug}`;
    }
    this.skip = (this.page - 1) * this.limit;
    this.api.category({ parent: this.parentCategory.slug, child: this.childCategory.slug, skip: this.skip, limit: this.limit })
      .subscribe((res: Response.BaseResponse) => {

        if (res.success === true) {
          this.result = res.data;
          this.categories = this.categories.concat(res.data.data);
          this.parentCategory.name = res.data.parentTitle;
          this.childCategory.name = res.data.title;
          this.helper
            .setCanonical(canUrl);
          if (res.data.metaTitle) {
            this.helper.setTitle(res.data.metaTitle);
            this.helper.updateMeta({
              title: res.data.metaTitle,
              description: res.data.metaDescription,
              keywords: res.data.metaKeywords.join(),
              imageURL: res.data.image,
              url: `https://www.spotboye.com${this.api.getLangURL()}/${this.parentCategory.slug}/${this.childCategory.slug}/page/${this.page}`,
              locale: ((this.api.getLang() === 'hi') ? 'hi_IN' : 'en_US')

            });
          }

          this.createWebpageSchema();
          if (this.firstLoad) {
            this.communicate.send({ type: InterCommunicateService.LOGO_ALT, payload: res.data.metaTitle });
            this.onload.loaded(true);
          } else {
            this.ga
              .sendTrack(`https://www.spotboye.com${this.api.getLangURL()}/${this.parentCategory.slug}/${this.childCategory.slug}/page/${this.page}`);
          }
          this.loaded = true;
        } else {
          this.router.navigate(['not-found']);

        }
      });
  }
}
