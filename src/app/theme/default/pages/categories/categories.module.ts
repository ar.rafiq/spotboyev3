import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { Routes, RouterModule } from '@angular/router';
import { ArticleComponent } from '../article/article.component';
import { SafeHtmlPipe } from '../../../../providers/application/utils/safe-html.pipe';
import { ArticleFragmentComponent } from '../../fragments/article/article/article.fragment';
import { VideoFragmentComponent } from '../../fragments/article/video/video.fragment';
import { DefaultModule } from '../../default.module';
import { PhotoFragmentComponent } from '../../fragments/article/photo/photo.fragment';
import { VendorsModule } from '../../../../vendors/vendors.module';


const categoriesRoute: Routes = [
  {
    path: '',
    component: ParentComponent
  },
  {
    path: 'page/:page',
    component: ParentComponent
  },
  {
    path: ':child',
    component: ChildComponent
  },
   {
    path: ':child/page/:page',
    component: ChildComponent
  },
  {
    path: ':child/:title/:articleID',
    component: ArticleComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(categoriesRoute),
    DefaultModule,
    VendorsModule
  ],
  exports: [RouterModule, ParentComponent, ChildComponent, ArticleComponent, SafeHtmlPipe,
    ArticleFragmentComponent, VideoFragmentComponent, PhotoFragmentComponent],
  declarations: [ParentComponent, ChildComponent, ArticleComponent, SafeHtmlPipe,
    ArticleFragmentComponent, VideoFragmentComponent, PhotoFragmentComponent]

})
export class CategoriesModule { }
