import { Component, OnInit, Inject, PLATFORM_ID, ElementRef, OnDestroy } from '@angular/core';
import { API } from '../../../../../providers/webservice/api.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Helper } from '../../../../../providers/application/utils/helper.service';
import { isPlatformBrowser } from '@angular/common';
import { DOCUMENT } from '@angular/platform-browser';
import { GAService } from '../../../../../vendors/google-analytics/ga.service';
import { ContentOnloadService } from '../../../../../events/content-onload/content-onload.service';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { InterCommunicateService } from '../../../../../events/inter-communicate/inter-communicate.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit, OnDestroy {

  categories: Array<any> = [];
  loaded: Boolean = false;
  langCat: Boolean = false;
  tabs: Array<Application.Tabs> = [];
  parentCategory: any = { name: '', slug: '' };
  result: any;
  page: any = 1;
  limit: any = 30;
  skip: Number = 0;
  firstLoad: Boolean = true;
  navigationListener: Subscription;
  langURL: String = '';
  currentDisplay: String = 'punjabi';
  constructor(private api: API, private route: ActivatedRoute, private helper: Helper, @Inject(PLATFORM_ID) private platform_id,
    @Inject(DOCUMENT) private doc, private elem: ElementRef, private router: Router, private onload: ContentOnloadService,
    private communicate: InterCommunicateService, private ga: GAService) {

  }
  isBrowser() {
    return isPlatformBrowser(this.platform_id);
  }
  ngOnInit() {
    this.route.data.subscribe(d => {
      this.api.setLang(d.lang);
      this.langURL = (d.lang === 'en') ? '' : d.lang + '/';
    });
    this.route.params.subscribe(param => {
      this.parentCategory.slug = param.parent;
      this.page = (param.page) ? Number.parseInt(param.page) : 1;
      this.langCat = (this.parentCategory.slug == 'regional' || this.parentCategory.slug == 'marathi' || this.parentCategory.slug == 'bengali' || this.parentCategory.slug == 'pollywood') ? true : false;
      this.currentDisplay = (this.parentCategory.slug == 'regional') ? 'pollywood' : this.parentCategory.slug;
      this.loadArticles();
    });

    this.api.tabs().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        res.data.forEach(tab => {
          this.tabs.push({ name: tab.name, id: `tab-${tab.name}-1`, data: tab.data });
        });
      }

    });

    this.navigationListener = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      this.categories = [];
    });
  }
  ngOnDestroy() {
    if (this.navigationListener) {
      this.navigationListener.unsubscribe();
    }
  }
  updateMeta() {
    // for setting canonical url when primary category hitted without page (this.page = 1)
    let canUrl = `https://www.spotboye.com${this.api.getLangURL()}/${this.parentCategory.slug}/page/${this.page}`;
    if (this.page === 1 || this.page === 0) {
      canUrl = `https://www.spotboye.com${this.api.getLangURL()}/${this.parentCategory.slug}`;
    }
    this.helper.setTitle(this.result.metaTitle);
    this.helper.setCanonical(canUrl);
    this.helper.updateMeta({
      title: this.result.metaTitle,
      description: this.result.metaDescription,
      keywords: this.result.metaKeywords.join(),
      imageURL: this.result.image,
      url: `https://www.spotboye.com${this.api.getLangURL()}/${this.parentCategory.slug}/page/${this.page}`,
      locale: ((this.api.getLang() === 'hi') ? 'hi_IN' : 'en_US')
    });
  }
  createWebpageSchema() {
    const data = {
      '@context': 'http://schema.org',
      '@type': 'WebPage',
      'name': this.parentCategory.name,
      'description': this.result.metaDescription,
      'url': '/' + this.parentCategory.slug,
      'publisher': {
        '@type': 'Organization',
        'name': 'SpotboyE',
        'url': 'https://www.spotboye.com/',
        'logo': {
          '@type': 'ImageObject',
          'contentUrl': '/assets/images/flat_logo.png'
        }
      }
    };

    const schemaEl = this.doc.createElement('script');
    schemaEl.type = `application/ld+json`;
    schemaEl.text = `${JSON.stringify(data)}`;
    setTimeout(() => {
      // console.log(this.elem.nativeElement.getElementsByTagName('div')[0], 'DIV');
      this.elem.nativeElement.getElementsByTagName('div')[0].appendChild(schemaEl);
    }, 0);
  }


  loadMoreLink(prev) {
    return (prev) ? ((this.page > 1) ? `/${this.parentCategory.slug}/page/${this.page - 1}` : '') :
      `/${this.parentCategory.slug}/page/${this.page + 1}`;
  }
  loadMore() {
    this.firstLoad = false;
    this.page++;
    this.loadArticles();
  }
  loadArticles() {
    this.skip = (this.page - 1) * this.limit;
    this.api.category({ parent: this.parentCategory.slug, skip: this.skip, limit: this.limit }).subscribe((res: Response.BaseResponse) => {

      if (res.success === true) {
        this.result = res.data;
        this.categories = this.categories.concat(res.data.data);
        this.parentCategory.name = res.data.title;
        this.currentDisplay = (this.parentCategory.slug == 'regional') ? 'pollywood' : this.parentCategory.slug;
        this.updateMeta();
        this.createWebpageSchema();

        if (this.firstLoad) {
          this.communicate.send({ type: InterCommunicateService.LOGO_ALT, payload: res.data.metaTitle });
          this.onload.loaded(true);
        } else {
          this.ga.sendTrack(`https://www.spotboye.com${this.api.getLangURL()}/${this.parentCategory.slug}/page/${this.page}`);
        }
        this.loaded = true;

      } else {
        this.router.navigate(['not-found']);
      }
    });
  }
  showTab(tabId) {
    this.currentDisplay = tabId;
  }
}
