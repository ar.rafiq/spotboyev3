import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search.component';
import { DefaultModule } from '../../default.module';



const searchRoutes: Routes = [
  {
    path: '',
    component: SearchComponent
  } ,
  {
    path: ':keyword',
    component: SearchComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    DefaultModule,
    RouterModule.forChild(searchRoutes)
  ],
  declarations: []
})
export class SearchModule { }
