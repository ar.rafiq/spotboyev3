import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Helper } from '../../../../providers/application/utils/helper.service';
import { API } from '../../../../providers/webservice/api.service';
import { ContentOnloadService } from '../../../../events/content-onload/content-onload.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  list: Array<Response.ArticleList> = [];
  loaded: Boolean = false;
  keyword: String;
  dpkeyword: String;
  tabsloaded: Boolean = false;
  tabs: any = [];
  isTagList: Boolean = false;
  showMoreBtn: Boolean = true;
  catGot: Boolean = false;
  cse: any = '';
  page: any = 1;
  skip: any = 0;
  limit: any = 30;
  pageType: any = 'search'; // or tags
  navigationListener: Subscription;

  constructor(private api: API, private helper: Helper, private route: ActivatedRoute,
    private onload: ContentOnloadService, private sanitize: DomSanitizer, private router: Router) {
    // this.cse = this.sanitize.bypassSecurityTrustHtml('<gcse:searchresults-only></gcse:searchresults-only>');
    this.api.tabs().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        this.tabsloaded = true;
        res.data.forEach(tab => {
          this.tabs.push({ name: tab.name, id: `search-tab-${tab.name.replace(' ', '-')}`, data: tab.data });
        });
      }

    });
    this.navigationListener = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(() => {
      this.list = [];
    });
  }
  ngOnDestroy() {
    if (this.navigationListener) {
      this.navigationListener.unsubscribe();
    }
  }
  ngOnInit() {
    this.route.data.subscribe(d => {
      this.pageType = d.type;
      this.fetchData();
    });

  }
  loadMoreLink(prev) {
    if (this.isTagList) {
      return (prev) ? ((this.page > 1) ? `${this.api.getLangURL()}/tags/${this.keyword}/page/${this.page - 1}` : '') :
        `${this.api.getLangURL()}/tags/${this.keyword}/page/${this.page + 1}`;
    } else {
      return (prev) ? ((this.page > 1) ? `${this.api.getLangURL()}/search/${this.keyword}/page/${this.page - 1}` : '') :
        `${this.api.getLangURL()}/search/${this.keyword}/page/${this.page + 1}`;
    }

  }
  fetchMore() {
    this.page++;
    this.skip = this.limit * this.page;
    this.fetchData();
  }
  fetchData() {
    this.route.params.subscribe(params => {

      if (params.keyword) {
        this.keyword = decodeURIComponent(params.keyword);
        this.catGot = (this.keyword === 'bigg-boss-13') ? true : false;
        this.dpkeyword = this.keyword.replace(new RegExp('-', 'g'), ' ').toLowerCase();
        this.helper.setCanonical(`https://www.spotboye.com/${this.pageType}/${this.keyword}`);
        this.api.search({ keyword: params.keyword, skip: this.skip, limit: this.limit }).subscribe((result: Response.BaseResponse) => {
          this.onload.loaded(true);
          if (result.success === true) {
            this.loaded = true;
            this.list = this.list.concat(result.data.data);
            if (result.data.data.length < this.limit) {
              this.showMoreBtn = false;
            }
            // update meta
            this.helper.setTitle(result.data.metaTitle);
            this.helper.setCanonical(`https://www.spotboye.com/${this.pageType}/${this.keyword}`);
            this.helper.updateMeta({
              title: result.data.metaTitle,
              description: result.data.metaDescription,
              keywords: result.data.metaKeywords,
              imageURL: (result.data && result.data.image) ? result.data.image : 'https://static.spotboye.com/uploads/spot-icon_2019-4-17-12-42-49.jpg',
              url: `https://www.spotboye.com/${this.pageType}/${this.keyword}`
            });
          } else {
            this.showMoreBtn = false;
          }
        });
      } else {

        this.route.queryParams.subscribe(query => {

          if (query.q) {
            this.keyword = query.q;
            this.api.search({ keyword: this.keyword, skip: this.skip, limit: this.limit }).subscribe((result: Response.BaseResponse) => {
              this.onload.loaded(true);
              if (result.success === true) {
                this.loaded = true;
                if (result.data.data.length < this.limit) {
                  this.showMoreBtn = false;
                }
                this.list = this.list.concat(result.data.data);

                // update meta
                this.helper.setTitle(result.data.metaTitle);
                this.helper.setCanonical(`https://www.spotboye.com/${this.pageType}/${this.keyword}`);
                this.helper.updateMeta({
                  title: result.data.metaTitle,
                  description: result.data.metaDescription,
                  keywords: result.data.metaKeywords,
                  imageURL: (result.data && result.data.image) ? result.data.image : 'https://static.spotboye.com/uploads/spot-icon_2019-4-17-12-42-49.jpg',
                  url: `https://www.spotboye.com/${this.pageType}/${this.keyword}`
                });


              } else {
                this.showMoreBtn = false;
              }
            });
          } else {

            this.isTagList = true;
            this.showMoreBtn = false;
            this.helper.setCanonical('https://www.spotboye.com/tags');
            this.api.tags().subscribe((result: Response.BaseResponse) => {
              this.onload.loaded(true);
              if (result.success === true) {
                this.list = result.data;
              }
            });
          }
        });
      }
    });
  }
}
