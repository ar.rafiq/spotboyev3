import { Component, OnInit } from '@angular/core';
import { ContentOnloadService } from '../../../../../../app/events/content-onload/content-onload.service';
import { API } from '../../../../../../app/providers/webservice/api.service';
import { ActivatedRoute } from '@angular/router';
import { Helper } from '../../../../../../app/providers/application/utils/helper.service';

@Component({
  selector: 'app-author-profile',
  templateUrl: './author-profile.component.html',
  styleUrls: ['./author-profile.component.scss']
})
export class AuthorProfileComponent implements OnInit {
  list: any = [];
  profile: any = {};
  loaded: Boolean = false;
  noshowError = false; // isAuthorExist ?
  skip: any = 0;
  limit: any = 50;
  hideMorePost: Boolean = false;
  authorName: any = '';
  constructor(private onload: ContentOnloadService, private api: API, private route: ActivatedRoute, private helper:Helper) {

  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.authorName = params.name;
      this.loadMorePost();
    });
  }

  loadMorePost() {
    this.api.authorProfile({ name: this.authorName, limit: this.limit, skip: this.skip }).subscribe((response) => {
      this.onload.loaded(true);

      if (response.success === true) {
        this.hideMorePost = (response.data.articles.length < this.limit);
        this.skip += this.limit;
        this.profile = {
          name: response.data.name, email: response.data.email,
          twitter: response.data.twitterLink, image: response.data.profilePic
        };
        this.list = this.list.concat(response.data.articles);
        this.helper.setTitle(`Articles by ${this.profile.name} | SpotboyE`);
        this.helper.setCanonical(`https://www.spotboye.com/author/${this.authorName}`);
        this.helper.updateMeta({
          title: `Articles by ${this.profile.name} | SpotboyE`,
          description: `Find out articles by ${this.profile.name} at SpotboyE.com`,
          keywords: 'spotboye,authors',
         url: `https://www.spotboye.com/author`,
         imageURL: this.profile.image,
        });
      } else {
        this.noshowError = true;
      }
    });
  }


}
