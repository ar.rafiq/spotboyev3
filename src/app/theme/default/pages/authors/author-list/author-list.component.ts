import { Component, OnInit } from '@angular/core';
import { ContentOnloadService } from '../../../../../../app/events/content-onload/content-onload.service';
import { API } from '../../../../../../app/providers/webservice/api.service';
import { Helper } from '../../../../../providers/application/utils/helper.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.scss']
})
export class AuthorListComponent implements OnInit {
  authorList: any = {};
  alphabets: String = '';
  langURL: String = '';
  constructor(private api: API, private helper: Helper, private route: ActivatedRoute, private onload: ContentOnloadService) {

  }

  ngOnInit() {
    this.route.queryParams.subscribe(qry => {
      if (qry.sort) {
        this.alphabets = qry.sort;
      }
      this.loadAuthors(this.alphabets);
    });
  }

  loadAuthors(_keywords) {
    this.api.authors(_keywords).subscribe((res: Response.BaseResponse) => {
      this.onload.loaded(true);
      if (res.success === true) {
        this.authorList = res.data;
        this.helper.setTitle('SpotboyE Authors');
        this.helper.setCanonical(`https://www.spotboye.com/author`);
        this.helper.updateMeta({
          title: 'Authors | Profiles of content writers & editors at SpotboyE',
          description: 'Authors – Find Profiles of Content Writers and Editors at SpotboyE.com',
          keywords: 'spotboye,authors',
         url: `https://www.spotboye.com/author`
        });
      }
    });
  }

}
