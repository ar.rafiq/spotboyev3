import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultModule } from '../../default.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthorListComponent } from './author-list/author-list.component';
import { AuthorProfileComponent } from './author-profile/author-profile.component';
import { VendorsModule } from '../../../../vendors/vendors.module';
import { FormsModule } from '@angular/forms';

const authorsRoute: Routes = [
  {
    path: '',
    component: AuthorListComponent
  },
  {
    path: ':name',
    component: AuthorProfileComponent
  },

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(authorsRoute),
    DefaultModule,
    VendorsModule
  ],
  exports: [RouterModule, AuthorListComponent, AuthorProfileComponent],
  declarations: [AuthorListComponent, AuthorProfileComponent]
})
export class AuthorsModule { }
