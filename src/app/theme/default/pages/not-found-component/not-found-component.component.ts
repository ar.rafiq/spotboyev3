import { Component, OnInit, PLATFORM_ID, Inject, APP_ID, Optional } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { RESPONSE } from '@nguniversal/express-engine/tokens';
import { Response } from 'express';
import { isPlatformBrowser } from '@angular/common';
import { ContentOnloadService } from '../../../../events/content-onload/content-onload.service';
@Component({
  selector: 'app-not-found-component',
  templateUrl: './not-found-component.component.html',
  styleUrls: ['./not-found-component.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string,
    @Optional() @Inject(RESPONSE) private response,
    private titleService: Title, private contentOnLoad:ContentOnloadService) {
    this.titleService.setTitle('Spotboye - Page Not Found');
  }

  ngOnInit() {
    this.contentOnLoad.loaded(true);
    if (!isPlatformBrowser(this.platformId)) {
      this.response.status(404);
    }
  }

}
