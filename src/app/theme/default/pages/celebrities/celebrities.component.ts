import { Component, OnInit } from '@angular/core';
import { API } from '../../../../providers/webservice/api.service';
import { Helper } from '../../../../providers/application/utils/helper.service';
import { ActivatedRoute } from '@angular/router';
import { ContentOnloadService } from '../../../../events/content-onload/content-onload.service';

@Component({
  selector: 'app-celebrities',
  templateUrl: './celebrities.component.html',
  styleUrls: ['./celebrities.component.scss']
})
export class CelebritiesComponent implements OnInit {
  searchCeleb: any;
  celebList: any = {};
  tabsloaded: Boolean = false;
  tabs: any = [];
  alphabets: String = 'A';
  langURL: String = '';
  constructor(private api: API, private helper: Helper, private route: ActivatedRoute, private onload: ContentOnloadService) {
    this.api.tabs().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        this.tabsloaded = true;
        res.data.forEach(tab => {
          this.tabs.push({ name: tab.name, id: `tab-${tab.name.replace(' ', '-')}`, data: tab.data });
        });
      }

    });
  }

  ngOnInit() {
    this.langURL = this.api.getLangURL();
    this.route.queryParams.subscribe(qry => {
      if (qry.sort) {
        this.alphabets = qry.sort;
      }
      this.loadCelebs(this.alphabets);
    });
  }

  loadCelebs(_keywords) {
    this.api.celebrity(_keywords).subscribe((res: Response.BaseResponse) => {
      this.onload.loaded(true);
      if (res.success === true) {
        this.celebList = res.data;
        this.helper.setTitle(this.celebList.metaTitle);
        this.helper.setCanonical(`https://www.spotboye.com/celebrity`);
        this.helper.updateMeta({
          title: this.celebList.metaTitle,
          description: this.celebList.metaDescription,
          keywords: this.celebList.metaKeywords,
         url: `https://www.spotboye.com/celebrity`
        });
      }
    });
  }

}
