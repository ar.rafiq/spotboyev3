import { Component, OnInit } from '@angular/core';
import { API } from '../../../../../providers/webservice/api.service';
import { Helper } from '../../../../../providers/application/utils/helper.service';
import { ActivatedRoute } from '@angular/router';
import { ContentOnloadService } from '../../../../../events/content-onload/content-onload.service';

@Component({
  selector: 'app-celebrity-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class CelebrityProfileComponent implements OnInit {
  profile: Application.CelebrityDetails = { name: '' };
  list: Array<Response.ArticleList>;
  loaded: Boolean = false;
  name: String;
  tabsloaded: Boolean = false;
  tabs: any = [];
  constructor(private api: API, private helper: Helper, private route: ActivatedRoute, private onload:ContentOnloadService) {
    this.api.tabs().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        this.tabsloaded = true;
        res.data.forEach(tab => {
          this.tabs.push({ name: tab.name, id: `tab-${tab.name.replace(' ', '-')}`, data: tab.data });
        });
      }

    });
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.api.celebrityProfile(params.name).subscribe((result: Response.BaseResponse) => {
        if (result.success === true) {
          this.loaded = true;
          this.profile.name = result.data.name;
          this.profile.thumbnail = result.data.thumbnail;
          this.profile.aboutceleb = result.data.aboutceleb;
          this.list = result.data.news;
          this.helper.setTitle(result.data.metaTitle);
          this.helper.setCanonical(`https://www.spotboye.com/celebrity/${params.name}`);
          this.helper.updateMeta({
            title: result.data.metaTitle,
            description: result.data.metaDescription,
            keywords: result.data.metaKeywords,
            imageURL: this.profile.thumbnail.toString(),
            url: `https://www.spotboye.com/celebrity/${params.name}`
          });
        }
        this.onload.loaded(true);
      });
    });

  }

}
