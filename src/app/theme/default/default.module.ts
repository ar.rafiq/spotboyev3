import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CelebritiesComponent } from './pages/celebrities/celebrities.component';
import { CelebrityProfileComponent } from './pages/celebrities/profile/profile.component';
import { BottomCreditComponent } from './components/bottom-credit/bottom-credit.component';
import { ReplacePipe } from '../../providers/application/utils/replace.pipe';
import { CardComponent } from './components/cards/card-item/card.component';
import { BoxAdComponent } from './components/box-ad/box-ad.component';
import { CardListItemComponent } from './components/cards/card-list-item/card-list-item.component';
import { SidemenuComponent } from './fragments/sidemenu/sidemenu.component';
import { HttpClientModule } from '@angular/common/http';
import { TabsComponent } from './components/tabs/tabs.component';
import { RouterModule } from '@angular/router';
import { CardBannerComponent } from './components/cards/banner/banner.component';
import { TimeagoPipe } from '../../providers/application/utils/timeago.pipe';
import { HeaderComponent } from './fragments/header/header.fragment';
import { VendorsModule } from '../../vendors/vendors.module';
import { FormsModule } from '@angular/forms';
import { InsComponent } from './pages/ins/ins.component';
import { NotFoundComponent } from './pages/not-found-component/not-found-component.component';
import { SearchComponent } from './pages/search/search.component';
import { SiteMaintenanceComponent } from './pages/site-maintenance/site-maintenance-component.component';
import { DeferLoadModule } from '@trademe/ng-defer-load';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    VendorsModule,
    FormsModule,
    DeferLoadModule
  ],
  declarations: [CelebritiesComponent, CelebrityProfileComponent, SidemenuComponent,
    HeaderComponent, BottomCreditComponent, CardComponent, BoxAdComponent, CardListItemComponent, TabsComponent, CardBannerComponent
    , TimeagoPipe, InsComponent, NotFoundComponent, SearchComponent, SiteMaintenanceComponent, ReplacePipe],
  exports: [CelebritiesComponent, CelebrityProfileComponent, SidemenuComponent,
    HeaderComponent, BottomCreditComponent, CardComponent, BoxAdComponent, CardListItemComponent, TabsComponent, RouterModule,
    CardBannerComponent, TimeagoPipe, InsComponent, SiteMaintenanceComponent, ReplacePipe, DeferLoadModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DefaultModule { }
