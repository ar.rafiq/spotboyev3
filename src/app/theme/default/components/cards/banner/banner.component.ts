import { Component, OnInit, Input } from '@angular/core';
import { Helper } from '../../../../../providers/application/utils/helper.service';

@Component({
  selector: 'app-card-banner',
  templateUrl: './banner.component.html'
})
export class CardBannerComponent implements OnInit {
  @Input('content') content: any;
  constructor(private helper: Helper) { }

  ngOnInit() {
  }

}
