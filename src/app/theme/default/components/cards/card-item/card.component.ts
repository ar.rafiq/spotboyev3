import { Component, OnInit, Input } from '@angular/core';
import { Helper } from '../../../../../providers/application/utils/helper.service';
import { trigger, state, style, transition, animate, query, stagger, keyframes } from '@angular/animations';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input('size') size: String;
  @Input('selector') selector: String;
  @Input('content') content: any; // Response.Article
  @Input('number') number: any;
  constructor(private helper: Helper) { }

  ngOnInit() {
  }

}
