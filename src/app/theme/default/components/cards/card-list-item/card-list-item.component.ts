import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-list-item',
  templateUrl: './card-list-item.component.html',
  styleUrls: ['./card-list-item.component.scss']
})
export class CardListItemComponent implements OnInit {
@Input('size') size;
  constructor() { }

  ngOnInit() {
  }

}
