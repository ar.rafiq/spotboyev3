import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomCreditComponent } from './bottom-credit.component';

describe('BottomCreditComponent', () => {
  let component: BottomCreditComponent;
  let fixture: ComponentFixture<BottomCreditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomCreditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
