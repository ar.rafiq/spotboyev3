import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxAdComponent } from './box-ad.component';

describe('BoxAdComponent', () => {
  let component: BoxAdComponent;
  let fixture: ComponentFixture<BoxAdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxAdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
