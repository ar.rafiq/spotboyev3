import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html'
})
export class TabsComponent implements OnChanges {

  showTrendingLeft: Boolean = false;

  @Input() tabscontent: any = [];
  currentDisplay: String;
  constructor() {

  }

  ngOnInit() {
    setTimeout(() => {
      this.showTrendingLeft = true;
    }, 8000);
  }

  ngOnChanges() {
    setTimeout(() => {
      if (this.tabscontent.length > 0) {
        this.currentDisplay = this.tabscontent[0].id;
        this.showTab(this.currentDisplay);
      }
    }, 10);

  }

  showTab(tabId) {
    this.currentDisplay = tabId;
  }
}
