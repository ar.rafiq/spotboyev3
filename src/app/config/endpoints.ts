export class Endpoints {
    private static HOST = 'https://apiv3.spotboye.com/web';
    // private static HOST = 'http://localhost:8080/web';
    public static LOGIN = '/auth/email/login';
    public static DASHBOARD = '/dashboard';
    public static MAINCATEGORY = '/:parent?skip=:skip&limit=:limit';
    public static ARTICLE = '/article/:article_id';
    public static SUBCATEGORY = '/:parent/:child?skip=:skip&limit=:limit';
    public static MENU = '/menus';
    public static TAGS = '/tags';
    public static TABS = '/sidebar/tabs';
    public static AUTHORS = '/author';
    public static SEARCH_AUTHORS = '/author/search/:keyword';
    public static AUTHOR_PROFILE = '/author/:name?skip=:skip&limit=:limit';
    public static CELEBRITY = '/celebrity/search/:keyword';
    public static CELEB_PROFILE = '/celebrity/:celebrity';
    public static SEARCH = '/search/:keyword?skip=:skip&limit=:limit';
    public static MORE = Endpoints.ARTICLE + '/more/:type';

    constructor() {

    }
    public static getURI(endpoint: any, param?: any, lang?: any) {
        const host_url: any = (lang) ? Endpoints.HOST + '/' + lang : Endpoints.HOST;
        if (endpoint) {
            if (typeof param !== 'object') {
                return host_url + endpoint.replace(':param', param);
            } else {
                let _url = host_url + endpoint;
                Object.keys(param).forEach(function (v, i) {
                    _url = _url.replace(':' + v, param[v]);
                });
                return _url;
            }
        } else {
            return host_url;
        }
    }
}
