import { Component, OnInit, OnDestroy, Inject, PLATFORM_ID, Input, ElementRef } from '@angular/core';
import { ScriptLoaderService } from './providers/application/utils/script-loader.service';
import { Router, NavigationEnd, NavigationStart, ActivatedRoute } from '@angular/router';
import { filter, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import * as $ from 'jquery';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';
import { GAService } from './vendors/google-analytics/ga.service';
import { Helper } from './providers/application/utils/helper.service';
import { MenuService } from './events/menu/menu.service';
import { ContentOnloadService } from './events/content-onload/content-onload.service';
import { API } from './providers/webservice/api.service';
declare const FB;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'spotboyev3';
  menus: Array<any> = [];
  listenersSubscription: Array<Subscription> = [];
  window: any = {};
  showShell: Boolean = true;
  shellType: String = 'category';
  currentLang: String = 'en';
  @Input('lang') lang: String;
  screenWidth: any = 728;
  constructor(private scriptLoader: ScriptLoaderService, private router: Router, @Inject(PLATFORM_ID) private platformId,
    @Inject(DOCUMENT) private document, private ga: GAService, private helper: Helper, private menusService: MenuService,
    private onload: ContentOnloadService, public api: API, private route: ActivatedRoute, private el: ElementRef) {

    this.ga.track();

  }

  ngOnInit() {

    if (this.el.nativeElement.lang) {
      this.api.setLang(this.el.nativeElement.lang);
      this.currentLang = this.el.nativeElement.lang;
      this.menusService.fetch(this.el.nativeElement.lang).listen().subscribe(menus => this.menus = menus);
    }

    this.onload.onload().subscribe(loaded => this.showShell = !loaded);



    if (this.isBrowser()) {
      this.window = window;
      // Load script async
      this.scriptLoader.loadScript('head', 'https://platform.twitter.com/widgets.js', true);
      // commented as ritesh 12 sept,2019
      // this.scriptLoader.loadScript('head', '//sdk.adspruce.com/1/adspruce.js?pid=4056&sid=1', true);
      //  this.scriptLoader.loadScript('head', '//impulse.forkcdn.com/pub/spotboye/spotboye/generic.js', true);
      // this.scriptLoader.loadScript('head', 'https://connect.facebook.net/en_US/sdk.js', true);
      // this.scriptLoader.loadScript('head', 'https://www.google-analytics.com/analytics.js', true);
      // this ad is for catfish and incontent
      // this.scriptLoader.loadScript('head', 'https://securepubads.g.doubleclick.net/tag/js/gpt.js', true);
      // this ad is for silverpush
      // this.scriptLoader.loadScript('head', 'https://www.youtube.com/iframe_api', true);
      // this.scriptLoader.loadScript('head', 'https://ap.edelmansp.co.in/javascripts/scdn.min.js', true);
      // this ad is for outstream
      // this.scriptLoader.loadScript('head', 'https://outstream.playstream.media/domain/327dbdd9-6544-4b87-9864-0f3797ef2195.js', true);

      // Load sctyles async
      this.scriptLoader.loadStyles('head', 'https://fonts.googleapis.com/css?family=Oswald');
      // by ruksana 20 dec,2019
      // this.scriptLoader.loadScript('head', '//c.jsrdn.com/s/cs.js?p=22793');

      // Listen to loaded script
      this.scriptLoader.onLoad$.subscribe(url => {
        switch (url) {
          case 'https://platform.twitter.com/widgets.js':
            setTimeout(() => {
              this.window.twttr.widgets.load();
            }, 0);
            break;

          case 'https://connect.facebook.net/en_US/sdk.js':
            // this.window.fbAsyncInit = function () {
            //   FB.init({
            //     appId: '1823404977913088',
            //     cookie: true,
            //     xfbml: true,
            //     version: 'v2.8'
            //   });

            //   FB.AppEvents.logPageView();
            // };
            // this.window.fbAsyncInit();
            // FB.getLoginStatus(function (response) {
            //   console.log(response,"FB response");
            // });
            break;

        }
      });

      // bring window to top on Navigation change
      this.listenersSubscription['navigationEndListener'] = this.router.events.pipe(filter((event) => event instanceof NavigationEnd))
        .subscribe(() => {
          window.scrollTo(0, 0);
          (this.window.COMSCORE && this.window.COMSCORE.beacon({c1: "2", c2: "19264324"}));
          });

      this.listenersSubscription['navigationStartListener'] = this.router.events.pipe(filter((event) => event instanceof NavigationStart))
        .subscribe((event: NavigationStart) => {
          // Add exception to show placeholder or show different type of placeholder based on url
          switch (event.url) {
            case '/not-found':
              this.onload.loaded(true);

              break;
            case '/':
              this.shellType = 'dashboard';
              this.onload.loaded(false);
              break;
            default:
              this.shellType = 'category';
              this.onload.loaded(false);
          }
          if (event.url.indexOf('/footer') !== -1) {
            this.onload.loaded(true);
          }
          if (event.url.indexOf('/hi/') !== -1) {
            this.api.setLang('hi');
          } else {
            this.api.setLang('en');

          }

        });


      // setTopHeight Main container
      const _adHeight = this.document.getElementById('mvp-leader-wrap').offsetHeight;
      this.document.getElementById('mvp-site-wall').style.top = `${_adHeight}px`;


      this.screenWidth = this.window.screen.width;


    }


  }

  ngOnDestroy() {

    if (this.listenersSubscription && this.listenersSubscription.length > 0) {
      this.listenersSubscription.forEach((l, i) => {
        l.unsubscribe();
      });

    }
  }
  isBrowser() {
    return isPlatformBrowser(this.platformId);
  }

  showHeaderOnScroll() {
    if (this.isBrowser()) {
      const leaderHeight = $('#mvp-leader-wrap').outerHeight();
      const logoHeight = $('#mvp-main-nav-top').outerHeight();
      const botHeight = $('#mvp-main-nav-bot').outerHeight();
      const navHeight = $('#mvp-main-head-wrap').outerHeight();
      const headerHeight = navHeight + leaderHeight;
      const aboveNav = leaderHeight + logoHeight;
      const totalHeight = logoHeight + botHeight;
      let previousScroll = 0;
      $(window).scroll(function (event) {
        const scroll = $(this).scrollTop();
        if ($(window).scrollTop() > aboveNav) {
          $('#mvp-main-nav-top').addClass('mvp-nav-small');
          $('#mvp-main-nav-bot').css('margin-top', logoHeight);
        } else {
          $('#mvp-main-nav-top').removeClass('mvp-nav-small');
          $('#mvp-main-nav-bot').css('margin-top', '0');
        }
        if ($(window).scrollTop() > headerHeight) {
          $('#mvp-main-nav-top').addClass('mvp-fixed');
          $('#mvp-main-nav-bot').addClass('mvp-fixed1');
          $('#mvp-main-body-wrap').css('margin-top', totalHeight);
          $('#mvp-main-nav-top').addClass('mvp-fixed-shadow');
          $('.mvp-fly-top').addClass('mvp-to-top');
          if (scroll < previousScroll) {
            $('#mvp-main-nav-bot').addClass('mvp-fixed2');
            $('#mvp-main-nav-top').removeClass('mvp-fixed-shadow');
          } else {
            $('#mvp-main-nav-bot').removeClass('mvp-fixed2');
            $('#mvp-main-nav-top').addClass('mvp-fixed-shadow');
          }
        } else {
          $('#mvp-main-nav-top').removeClass('mvp-fixed');
          $('#mvp-main-nav-bot').removeClass('mvp-fixed1');
          $('#mvp-main-nav-bot').removeClass('mvp-fixed2');
          $('#mvp-main-body-wrap').css('margin-top', '0');
          $('#mvp-main-nav-top').removeClass('mvp-fixed-shadow');
          $('.mvp-fly-top').removeClass('mvp-to-top');
        }
        previousScroll = scroll;
      });
    }
  }
}