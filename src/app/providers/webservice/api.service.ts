import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Endpoints } from '../../config/endpoints';
import { LoginResponse } from '../../models/login/login-response';
// import { LocalStorage } from '../application/storage/local-storage.service';


@Injectable()
export class API {
  private lang: String = 'en';

  constructor(public http: HttpClient) { }

  setLang(lang: String) {
    this.lang = lang;
  }
  getLang() {
    return this.lang;
  }
  getLangURL() {
    return (this.lang === 'en') ? '' : '/hi';
  }
  login(data) {
    return this.http.post<Response.BaseResponse>(Endpoints.LOGIN, data);
  }
  dashboard() {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.DASHBOARD, '', this.getLang()));
  }
  article(id) {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.ARTICLE, { article_id: id }, this.lang));
  }

  category(payload) {
    let url = Endpoints.MAINCATEGORY;
    if (payload.child) {
      url = Endpoints.SUBCATEGORY;
    }
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(url, payload, this.lang));
  }
  menu() {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.MENU, '', this.lang));
  }
  tags() {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.TAGS, '', this.lang));
  }
  tabs() {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.TABS, '', this.lang));
  }
  celebrityProfile(name) {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.CELEB_PROFILE, { celebrity: name }, this.lang));
  }
  celebrity(keyword: String) {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.CELEBRITY, { keyword: keyword }, this.lang));
  }
  authorProfile(params) {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.AUTHOR_PROFILE, params));
  }
  authors(keyword) {

    return this.http.get<Response.BaseResponse>(Endpoints.getURI((keyword === '') ? Endpoints.AUTHORS : Endpoints.SEARCH_AUTHORS,
      { keyword: keyword }));
  }
  search(keyword) {
    return this.http.get<Response.BaseResponse>(Endpoints.getURI(Endpoints.SEARCH, keyword, this.lang));
  }
  more(_t, payload) {
    return this.http.post<Response.BaseResponse>(
      Endpoints.getURI(Endpoints.MORE, { type: _t.type, article_id: _t.id }, this.lang), payload);
  }

  dryRun() {
    return this.http.get('https://apiv2.spotboye.com/');
  }

}
