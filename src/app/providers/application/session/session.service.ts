import { Injectable } from '@angular/core';

import { LocalStorage } from '../storage/local-storage.service';

@Injectable()
export class Session {

  private USER_INFO = 'user_info';
  private currentSession = null;
  constructor(private storage: LocalStorage) { }

  public saveSession(_data) {
    this.currentSession = _data;
    this.storage.saveAsString(this.USER_INFO, _data);
  }

  private getSession() {
   // const _s = (this.storage.get(this.USER_INFO)) ? this.storage.get(this.USER_INFO) : {};
    return this.currentSession = JSON.parse(this.storage.get(this.USER_INFO));
  }

  public getUsername() {
    return this.getSession().name;
  }

  public getProfilePic() {
    return this.getSession().profilePic;
  }
  public isLoggedIn() {
    return this.getSession().isLoggedIn;
  }
}
