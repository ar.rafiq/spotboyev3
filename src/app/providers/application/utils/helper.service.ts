import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Title, Meta, MetaDefinition } from '@angular/platform-browser';

@Injectable()
export class Helper {

  constructor(@Inject(DOCUMENT) private doc, private title: Title, private meta: Meta, @Inject(PLATFORM_ID) private platform_id) { }

  isBrowser() {
    return isPlatformBrowser(this.platform_id);
  }

  makeURL(id, section, slug) {
    return section.parentSection + '/' + section.slug + '/' + slug + '/' + id;
  }
  stripURL(url) {
    return '/' + url.split('/').slice(3).join('/');
  }

  setCanonical(url) {

    if (url && this.doc.getElementById('canonical')) {

      this.doc.getElementById('canonical').setAttribute('href', url);
    }
  }
  setTitle(_title) {
    this.title.setTitle(_title);
  }
  updateMeta(metaData: Application.MetaData) {
    const title: MetaDefinition = { name: 'title', content: metaData.title };
    const twttitle: MetaDefinition = { name: 'twitter:title', content: metaData.title };
    const description: MetaDefinition = { name: 'description', content: metaData.description };
    const twtdescription: MetaDefinition = { name: 'twitter:description', content: metaData.description };
    const keywords: MetaDefinition = { name: 'keywords', content: metaData.keywords };
    const ogtitle: MetaDefinition = { property: 'og:title', content: metaData.title };
    const ogdescription: MetaDefinition = { property: 'og:description', content: metaData.description };
    const oglocale: MetaDefinition = { property: 'og:locale', content: (metaData.locale) ? metaData.locale : 'en_US' };

    const ogurl: MetaDefinition = { property: 'og:url', content: metaData.url };
    this.meta.updateTag(title);
    this.meta.updateTag(twttitle);
    this.meta.updateTag(description);
    this.meta.updateTag(twtdescription);
    this.meta.updateTag(keywords);
    this.meta.updateTag(ogtitle);
    this.meta.updateTag(ogurl);
    this.meta.updateTag(ogdescription);
    this.meta.updateTag(oglocale);

    if (metaData.imageURL) {
      const twtimage: MetaDefinition = { name: 'twitter:image', content: metaData.imageURL };
      const metaimage: MetaDefinition = { property: 'og:image', content: metaData.imageURL };
      this.meta.updateTag(metaimage);
      this.meta.updateTag(twtimage);
    }
  }
}
