import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeago'
})
export class TimeagoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return this.getMomentAgo(value);
  }
  getMomentAgo(_date) {
    _date = new Date(_date).getTime();
    const msPerMinute = 60 * 1000;
    const msPerHour = msPerMinute * 60;
    const msPerDay = msPerHour * 24;
    const msPerMonth = msPerDay * 30;
    const msPerYear = msPerDay * 365;

    const elapsed = (new Date().getTime()) - _date; // current - previous;

    /* 
    REMOVED TIMES AGO FEATURE ON REQUEST
    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' sec';
      } else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + ' mins';
      } else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + ' hrs';
      } else {
      }
  */
    // SHOW FULL DATE
    const monthNames = [
      'Jan', 'Feb', ' Mar',
      'Apr', 'May', 'Jun', 'Jul',
      'Aug', 'Sep', 'Oct',
      'Nov', 'Dec'
    ];

    const date = new Date(_date);
    const day = date.getDate();
    const monthIndex = date.getMonth();
    const year = date.getFullYear();

    return monthNames[monthIndex] + ' ' + day + ' ' + year + ', ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
  }

  /*else if (elapsed < msPerMonth) {
      return Math.round(elapsed/msPerDay) + ' days';
  }
 
  else if (elapsed < msPerYear) {
      return Math.round(elapsed/msPerMonth) + ' months';
  }
 
  else {
      return Math.round(elapsed/msPerYear ) + ' years';
  }*/



}
