import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ContentOnloadService {

  private onload$: BehaviorSubject<Boolean> = new BehaviorSubject(false);
  constructor() {

  }
  public onload() {
    return this.onload$;
  }
  public loaded(isLoaded: Boolean) {
    this.onload$.next(isLoaded);
  }


}
