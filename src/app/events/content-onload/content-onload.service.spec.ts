import { TestBed, inject } from '@angular/core/testing';

import { ContentOnloadService } from './content-onload.service';

describe('ContentOnloadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContentOnloadService]
    });
  });

  it('should be created', inject([ContentOnloadService], (service: ContentOnloadService) => {
    expect(service).toBeTruthy();
  }));
});
