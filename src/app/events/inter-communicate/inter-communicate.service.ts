import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterCommunicateService {
  public static LOGO_ALT = 'LOGO_ALT';
  public static PAGE_CONTENT = 'PAGE_CONTENT';

  public listen$: BehaviorSubject<CommunicationPayloadModel> = new BehaviorSubject(null);

  constructor() { }

  public send(payload: CommunicationPayloadModel) {
    this.listen$.next(payload);
  }
}

export interface CommunicationPayloadModel {
  type: String;
  payload: any;
}
