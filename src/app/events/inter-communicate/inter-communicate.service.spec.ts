import { TestBed, inject } from '@angular/core/testing';

import { InterCommunicateService } from './inter-communicate.service';

describe('InterCommunicateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InterCommunicateService]
    });
  });

  it('should be created', inject([InterCommunicateService], (service: InterCommunicateService) => {
    expect(service).toBeTruthy();
  }));
});
