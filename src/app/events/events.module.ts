import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuService } from './menu/menu.service';
import { ContentOnloadService } from './content-onload/content-onload.service';
import { InterCommunicateService } from './inter-communicate/inter-communicate.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [MenuService, ContentOnloadService, InterCommunicateService]
})
export class EventsModule { }
