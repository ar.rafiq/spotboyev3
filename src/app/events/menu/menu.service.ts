import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { API } from '../../providers/webservice/api.service';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private listener$: BehaviorSubject<any> = new BehaviorSubject([]);

  constructor(private api: API) {
  }
  fetch(_l: String) {
    if (_l !== '') {
      this.api.setLang(_l);
    }
    this.api.menu().subscribe((res: Response.BaseResponse) => {
      if (res.success === true) {
        const lang = this.api.getLang();
        this.listener$.next(res.data.map(_i => {
         _i.langURL = (lang === 'en') ? '' : '/hi';
         return _i;
        }));
      }
    });


    return this;
  }
 
  listen() {
    return this.listener$;
  }
}
