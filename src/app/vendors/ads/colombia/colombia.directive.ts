import { Directive, ElementRef, Input, Inject } from '@angular/core';
import { Helper } from '../../../providers/application/utils/helper.service';
import { AdsManagerService } from '../ads-manager.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { WINDOW } from '@ng-toolkit/universal';
declare var window;
@Directive({
  selector: '[appColombia]'
})
export class ColombiaDirective {
  @Input() id: string;
  constructor(@Inject(WINDOW) private _window, private helper: Helper,
    private elemRef: ElementRef, private ads: AdsManagerService, private router: Router) {

    if (this.helper.isBrowser()) {
      this.ads.init().subscribe(data => {
        if (data.window && data.type === AdsManagerService.TYPE_COLOMBIA) {
          // Render ads on first load
          this.renderAd(data.window);

          // checking page transition to refresh on every page
          this.router.events.pipe(filter((event) => event instanceof NavigationEnd))
            .subscribe(() => {
              this.renderAd(data.window);
            });
        }
      });
    }
  }
  renderAd(colombia) {
    console.log(this._window.screen,'screen');
    if (this._window.screen.width > 700) {
      this.elemRef.nativeElement.innerHTML = `<div id='clmbFixed' style='float:left;min-height:2px;width:100%;'
      data-slot='211038' data-position='1' data-section='0' data-ua='d' class='colombia'></div>`;
    } else {
      this.elemRef.nativeElement.innerHTML = `<div id="clmbFixed" style="min-height:2px;width:100%;"
       data-slot="211040" data-position="1" data-section="0" data-ua="m" class="colombia"></div>`;

    }

    if (colombia) {
      console.log('col', colombia);
      colombia.fns.push(function () {
        colombia.refresh('clmbFixed');
      });
    }
  }
}
