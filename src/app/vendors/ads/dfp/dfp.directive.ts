import { Directive, ElementRef, Input, OnChanges, PLATFORM_ID, Inject, ChangeDetectionStrategy } from '@angular/core';
import { AdsManagerService, AdsManagerModel } from '../ads-manager.service';
import { isPlatformBrowser } from '@angular/common';

@Directive({
  selector: '[appDfp]'
})

export class DfpDirective implements OnChanges {
  @Input() code: string;
  @Input() id: string;
  @Input() adWidth: any;
  @Input() adHeight: any;
  @Input() outofpage: Boolean;
  @Input() responsive: Boolean;
  private googletag: any;
  isBrowser: Boolean = false;
  deviceType = 'Desktop';
  constructor(private el: ElementRef, private ads: AdsManagerService, @Inject(PLATFORM_ID) private platformId) {
    this.isBrowser = isPlatformBrowser(this.platformId);
    this.deviceType = (this.isBrowser) ? ((window.screen.width >= 700) ? 'Desktop' : 'Mobile') : '';
  }
  ngOnChanges() {

    if (this.isBrowser) {
      // Get ads width&height based on screen size

      const width = (typeof this.adWidth === 'string' || typeof this.adWidth === 'number') ? this.adWidth : ((this.adWidth.length > 0) ?
        ((window.screen.width >= 700) ? this.adWidth[0] : this.adWidth[1]) : this.adWidth[0]);
      const height = (typeof this.adHeight === 'string' || typeof this.adWidth === 'number') ? this.adHeight : ((this.adHeight.length > 0) ?
        ((window.screen.width >= 700) ? this.adHeight[0] : this.adHeight[1]) : this.adHeight[0]);

      this.ads.init().subscribe((ack: AdsManagerModel) => {
        if (ack.type === AdsManagerService.TYPE_GPT && ack.loaded === true) {
          this.googletag = ack.window;
          this.googletag.cmd.push(() => {

            let slot;
            // Postfix Device name in Google Ads code for ex. Spotboye_Homepage_*_Desktop || Spotboye_Homepage_*_Mobile

            if (this.responsive === true) {
              this.code += `_${this.deviceType}`;
            }

            if (this.outofpage === true) {

              slot = this.googletag.defineOutOfPageSlot(this.code, [width, height], this.id);

            } else {

              slot = this.googletag.defineSlot(this.code, [Number(width), Number(height)], this.id);
            }

            if (slot) {
              const affTargeting = [];
              affTargeting.push((Math.floor(Math.random() * 20) + 1) + '');

              slot.addService(this.googletag.pubads());
                 slot.setTargeting('aff', affTargeting);
              //   slot.setTargeting('pagetype', ['test']);
            } else {
              //  slot.addService(this.googletag.pubads());
              this.googletag.pubads().refresh(this.id);
            }
            setTimeout(() => {
              // this.googletag.enableServices();
              this.googletag.display(this.id);
            }, 10);

          });
        }
      });
    }
  }
}
