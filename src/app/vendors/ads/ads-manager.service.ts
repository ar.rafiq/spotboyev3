import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ScriptLoaderService } from '../../providers/application/utils/script-loader.service';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AdsManagerService {
  public static TYPE_GPT = 'gpt';
  public static TYPE_COLOMBIA = 'colombia';
  private bootloader$: BehaviorSubject<AdsManagerModel> = new BehaviorSubject({ loaded: false, window: {} });
  private scriptSrc: Array<String> = [];
  private window: any;
  constructor(private scriptLoader: ScriptLoaderService, @Inject(PLATFORM_ID) private platforId) {
    if (isPlatformBrowser(this.platforId)) {
      this.window = (window) ? window : {};
      this.scriptSrc.push('https://www.googletagservices.com/tag/js/gpt.js');
      this.scriptSrc.push('//static.clmbtech.com/ctn/commons/js/colombia_v2.js');
      // this.scriptSrc.push('https://go.automatad.com/geo/LBb40Q/afihbs.js');

      scriptLoader.loadScripts('head', this.scriptSrc, true).then(scriptsLoaded => {
        // Send google aknowledgement
        const win: any = this.window;
        const googletag = win.googletag || {};
        googletag.cmd = googletag.cmd || [];
        googletag.cmd.push(() => {

          const pubads = googletag.pubads();
          pubads.enableAsyncRendering();
          pubads.enableVideoAds();
          pubads.setForceSafeFrame(false);
          pubads.setCentering(true);
          googletag.pubads().enableSingleRequest();
          // pubads.disableInitialLoad();
          googletag.enableServices();
          this.sendStatus({ type: AdsManagerService.TYPE_GPT, loaded: true, window: googletag });
        });


        const colombia = this.window.colombia || {};
        colombia.fns = colombia.fns || {};
        this.sendStatus({ type: AdsManagerService.TYPE_COLOMBIA, loaded: true, window: colombia });

        //Any 3rd part ADS
        
        // const _itId = setInterval(() => {
        //   if (this.window.googletag) {
        //     googletag.cmd.push(() => {
        //       const pubads = googletag.pubads();
        //     // pubads.enableAsyncRendering();
        //       pubads.enableVideoAds();
        //       pubads.setForceSafeFrame(false);
        //       pubads.setCentering(true);
        //      // pubads.disableInitialLoad();
        //       googletag.enableServices();
        //    });

        //     this.sendStatus({ type: AdsManagerService.TYPE_GPT, loaded: true, window: googletag });
        //     clearInterval(_itId);
        //   }
        // }, 10);

        // Send ads aknowledgement


      }).catch(err => {
        console.log('Error occurred while loading ads script', err);
      });
    }
  }
  private sendStatus(payload: AdsManagerModel) {
    this.bootloader$.next(payload);
  }
  init() {
    return this.bootloader$;
  }
}


export interface AdsManagerModel {
  type?: String;
  loaded?: Boolean;
  window?: any;

}
