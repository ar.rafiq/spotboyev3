import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DfpDirective } from './ads/dfp/dfp.directive';
import { AdsManagerService } from './ads/ads-manager.service';
import { GAService } from './google-analytics/ga.service';
import { ColombiaDirective } from './ads/colombia/colombia.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DfpDirective, ColombiaDirective],
  exports: [DfpDirective, ColombiaDirective],
  providers: [AdsManagerService, GAService]
})
export class VendorsModule { }
