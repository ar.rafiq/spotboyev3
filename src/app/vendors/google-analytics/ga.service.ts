import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

declare var gtag: Function;

@Injectable({
  providedIn: 'root'
})
export class GAService {

  trackID: String = 'UA-53522784-1';
  constructor(public router: Router) {
  }

  public track() {
    this.router.events.subscribe(event => {
      try {
        if (typeof gtag === 'function') {
          if (event instanceof NavigationEnd) {
            gtag('config', this.trackID, {
              'page_path': event.urlAfterRedirects
            });
          }
        }
      } catch (e) {
        console.log(e);
      }
    });
  }


  public sendTrack(url: String) {
    if (typeof gtag === 'function') {

      gtag('config', this.trackID, {
        'page_path': url
      });

    }
  }
  /**
   * this.emitEvent("bollywood", "viewMore", "user viewed more", 10);
   * @param {string} eventCategory
   * @param {string} eventAction
   * @param {string} eventLabel
   * @param {number} eventValue
   */
  public emitEvent(eventCategory: string,
    eventAction: string,
    eventLabel: string = null,
    eventValue: number = null) {
    if (typeof gtag === 'function') {
      gtag('event', eventAction, {
        'event_category': eventCategory,
        'event_label': eventLabel,
        'value': eventValue
      });
    }
  }

  public gaVideoEvent(eventCategory: string,
    eventAction: string,
    eventLabel: string = null,
    eventValue: string = null) {
      if (typeof gtag === 'function') {
        gtag('event', eventAction, {
          'event_category': eventCategory,
          'event_label': eventLabel,
          'value': eventValue
        });
      }
  }
}
