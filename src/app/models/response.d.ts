declare module Response {

    export interface MainSection {
        _id: string;
        color: string;
        parentSection: string;
        slug: string;
        title: string;
        parentSlug: string;
    }

    export interface Celebritytag {
        _id: string;
        thumbnail: string;
        name: string;
    }

    export interface Media {
        _id: string;
        file: string;
    }

    export interface Article {
        _id: string;
        type: string;
        lang: string;
        mainSection: MainSection;
        slug?: string;
        thumbnail: string;
        celebritytags: Celebritytag[];
        locale?: string;
        publishedAt: Date;
        modifiedAt?: Date;
        views?: number;
        authorName: string;
        shortBody: string;
        title: string;
        exclusive?: boolean;
        small?: string;
        shareURL: string;
        youtubeId?: string;
        buttonText: string;
        desc: string;
        media: Media[];
        name: string;
        moreVideos?: Array<any>;
        tags?: Array<String>;
        metaTitle?: string;
        metaKeywords?: any;
        metaDescription?: string; Í
    }
    export interface ArticleList {
        _id: string;
        type?: string;
        lang?: string;
        mainSection?: MainSection;
        thumbnail: string;
        publishedAt?: Date;
        shortBody?: string;
        title: string;
        exclusive?: boolean;
        small?: string;
        shareURL?: string;
    }
    export interface Section {
        section: number;
        sectionName: string;
        data: Article[];
    }

    export interface Dashboard {
        data: Section[];
    }
    export interface ArticleResponse {
        data: Article;
    }

    export interface SubMenu {
        _id: string;
        color?: string;
        slug: string;
        title: string;
    }

    export interface MainMenu {
        _id: string;
        color?: string;
        slug?: string;
        subMenu: SubMenu[];
        title: string;
        hasSubmenu?: boolean;
        type?: string;
    }

    export interface BaseResponse {
        success: Boolean,
        message: String,
        data: any;
    }

}

