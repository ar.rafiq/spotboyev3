
interface LoginPatern {
    attemptedArticles?: Array<string>;
    authToken?: string;
    emailVerification?: boolean;
    isAdmin?: boolean;
    isEditor?: boolean;
    name: string;
    profilePic: string;
    isLoggedIn?:boolean;
}
export interface LoginResponse {
    data: LoginPatern;
}
