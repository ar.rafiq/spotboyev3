declare module Application {

    export interface Tabs {
        name: String,
        id: String,
        data: Array<{}>
    }

    export interface CelebrityDetails {
        name: String,
        id?: String,
        aboutceleb?: String,
        thumbnail?: String
    }
    export interface MetaData {
        title: string,
        description: string,
        keywords?: string,
        imageURL?: string,
        url?: string,
        locale?:string
    }

}