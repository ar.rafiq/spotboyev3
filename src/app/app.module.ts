import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouteManagerModule } from './theme/route-manager.module';
import { VendorsModule } from './vendors/vendors.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UrlSerializer } from '@angular/router';
import CustomUrlSerialize from './providers/application/utils/custom-url-serialize';

const customUrlSerializer = new CustomUrlSerialize();
const CustomUrlSerializerProvider = {
  provide: UrlSerializer,
  useValue: customUrlSerializer
};
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgtUniversalModule,
    RouteManagerModule,
    VendorsModule
  ],
  providers: [CustomUrlSerializerProvider]
})
export class AppModule { }
