jQuery(document).ready(function($) {
"use strict";

//theiaStickySidebar 
$('[data-theiaStickySidebar-sidebarSelector]').each(function () {
  var $this = $(this);
  var sidebarSelector = JSON.parse($this.attr('data-theiaStickySidebar-sidebarSelector'));
  var options = JSON.parse($this.attr('data-theiaStickySidebar-options'));

  $(sidebarSelector).theiaStickySidebar(options);
});
  	// Fly-Out Navigation
	$(".mvp-fly-but-click").on('click', function(){
		$("#mvp-fly-wrap").toggleClass("mvp-fly-menu");
		$("#mvp-fly-wrap").toggleClass("mvp-fly-shadow");
  		$(".mvp-fly-but-wrap").toggleClass("mvp-fly-open");
  		$(".mvp-fly-fade").toggleClass("mvp-fly-fade-trans");
	});

	// Back to Top Button
    	var duration = 500;
    	$('.back-to-top').on('click', function(event) {
          event.preventDefault();
          $('html, body').animate({scrollTop: 0}, duration);
          return false;
    	});

 	// Search Toggle
 	$(".mvp-search-click").on('click', function(){
	   $("#mvp-search-wrap").toggleClass("mvp-search-toggle");
	   $("#search").trigger('focus');
  	});

 	// Mobile Social Toggle
 	$(".mvp-mob-soc-click").on('click', function(){
 	  $(".mvp-mob-soc-list").toggleClass("mvp-mob-soc-tog");
  	});

 	// Trending Toggle
 	$(".mvp-post-trend-but-click").on('click', function(){
 	  $("#mvp-post-trend-wrap").toggleClass("mvp-post-trend-tog");
  	});

   	// Comments Toggle
   	$(".mvp-com-click").on('click', function(){
	  $("#comments").show();
  	  $("#disqus_thread").show();
  	  $("#mvp-comments-button").hide();
  	});

   	// Continue Reading Toggle
   	$(".mvp-ad-rel-click").on('click', function(){
	  $("#mvp-content-main").css('max-height','none');
  	  $('#mvp-ad-rel-wrap').css('margin-top','20px');
  	  $("#mvp-ad-rel-top").hide();
  	});

	// Columns Toggle
	$('.mvp-feat1-right-wrap').each(function() {
		$(this).find(".mvp-tab-col-cont").hide(); //Hide all content
		$(this).find("ul.mvp-feat1-list-buts li.mvp-feat-col-tab").addClass("active").show(); //Activate first tab
		$(this).find(".mvp-tab-col-cont:first").show(); //Show first tab content
	});

	$("ul.mvp-feat1-list-buts li").on('click', function(e) {
		$(this).parents('.mvp-feat1-right-wrap').find("ul.mvp-feat1-list-buts li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(this).parents('.mvp-feat1-right-wrap').find(".mvp-tab-col-cont").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(this).parents('.mvp-feat1-right-wrap').find(activeTab).fadeIn(); //Fade in the active ID content

		e.preventDefault();
	});

	$("ul.mvp-feat1-list-buts li a").on('click', function(e) {
		e.preventDefault();
	});

	// Widget Columns Toggle
	$('.mvp-widget-tab-wrap').each(function() {
		$(this).find(".mvp-tab-col-cont").hide(); //Hide all content
		$(this).find("ul.mvp-feat1-list-buts li.mvp-feat-col-tab").addClass("active").show(); //Activate first tab
		$(this).find(".mvp-tab-col-cont:first").show(); //Show first tab content
	});

	$("ul.mvp-feat1-list-buts li").on('click', function(e) {
		$(this).parents('.mvp-widget-tab-wrap').find("ul.mvp-feat1-list-buts li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(this).parents('.mvp-widget-tab-wrap').find(".mvp-tab-col-cont").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(this).parents('.mvp-widget-tab-wrap').find(activeTab).fadeIn(); //Fade in the active ID content

		e.preventDefault();
	});

	$("ul.mvp-feat1-list-buts li a").on('click', function(e) {
		e.preventDefault();
	});

});