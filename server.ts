import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import { enableProdMode } from '@angular/core';
import { ngExpressEngine } from '@nguniversal/express-engine';
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';

import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as compression from 'compression';

import * as http from 'http';

// import * as got from 'got';

enableProdMode();

export const app = express();

app.use(compression());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// const DIST_FOLDER = join(process.cwd(), 'dist');

const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist/server/main');



app.get('/redirect/**', (req, res) => {
  const location = req.url.substring(10);
  res.redirect(301, location);
});

const getXMLFeed = function (req, res) {
  const options = {
    host: 'apiv3.spotboye.com',
    port: 80,
    path: '/web/xml?for=' + req.path,
    method: 'GET'
  };
  const request = http.request(options, function (result: any) {
    let _d = '';
    // res.setEncoding('utf8');
    res.setHeader('Content-Type', 'application/xml');
    result.on('data', function (chunk) {
      _d += chunk;
    });
    result.on('end', function () {

      res.send(_d);
    });
  });
  request.setHeader('User-Agent', (req.header('User-Agent')) ? req.header('User-Agent') : 'Google Chrome');
  request.on('error', function (e) {
    res.send(e.message);
  });
  request.end();
};
app.get('**.xml', (req, res) => {
  getXMLFeed(req, res);
});
app.get('/:lang?/feed/*', (req, res) => {
  getXMLFeed(req, res);
});
app.get('*.*', express.static('./dist/browser', {
  maxAge: '1y'
}));
app.get('/amp*', express.static('./dist', {
  maxAge: '1y'
}));

/** AMP RELATED STARTS */

// String.prototype.replacerec = function (pattern, what) {
//   var newstr = this.replace(pattern, what);
//   if (newstr == this)
//     return newstr;
//   return newstr.replacerec(pattern, what);
// };

app.get('/amp/:parent/:child/:title/:id', (req, res) => {
  //console.log('enetered here')
  const render = (json) => {
    //console.log(json)
    const data = JSON.parse(json);
    let _html = '';
    if (data.success === true) {
      const article = data.data;
      let menusHtml = (data.data.menu) ? data.data.menu : [];
      menusHtml = data.data.menu.map(item => {
        if (item.type === 'page') {
          return `<h4><a href='${item.url}'>${item.title}</a></h4>`;
        } else {
          return `<h4><a href='https://www.spotboye.com/${item.slug}'>${item.title}</a></h4>`;
        }

      }).join('');
      const replacerec = (pattern, what) => {
        const newstr = this.replace(pattern, what);
        if (newstr == this) {
          return newstr;
        }
        return newstr.replacerec(pattern, what);
      };

      const dTime = new Date(article.publishedAt).toTimeString();
      console.log('pub at->', article.publishedAt);

      const dtNew = new Date(article.publishedAt);

      console.log('tt', dtNew.toLocaleTimeString('en-US'));

      var aestTime = new Date(article.publishedAt).toLocaleString("en-US", {timeZone: "Australia/Brisbane"});

      const aestTimeNew  = new Date(aestTime);
      //console.log('AEST time: ', aestTimeNew.toLocaleString());

      var asiaTime = new Date(article.publishedAt).toLocaleString("en-US", {timeZone: "Asia/Shanghai"});
      const asiaTimeNew  = new Date(asiaTime);
      //console.log('Asia time: ', asiaTimeNew.toLocaleString());

      var usaTime = new Date(article.publishedAt).toLocaleString("en-US", {timeZone: "America/New_York"});
      const usaTimeNew = new Date(usaTime);
     // console.log('USA time: ', usaTimeNew.toLocaleString());

      var indiaTime = new Date(article.publishedAt).toLocaleString("en-US", {timeZone: "Asia/Kolkata"});
      const indiaTimeNew = new Date(indiaTime);
      const indiaTimeOnly = indiaTimeNew.toTimeString();
      //console.log('India time: ', indiaTimeNew.toLocaleString());

      const dTimearrNew = indiaTimeOnly.split(" ");

      const dTimearr = dTime.split(" ");

      const getClosingTags: any = (htmlString) => {
        return (htmlString.match(/<p/g) || []).length - (htmlString.match(/<\/p>/g) || []).length;
      };
      const moveTagToRoot = (match, p1, offset, htmlString) => {
        const closingTags = getClosingTags(htmlString.substring(0, offset));
        let replacement = p1;
        for (let i = 1; i <= closingTags; i++) {
          replacement = '</p>' + replacement + '<p>';
        }
        return replacement;
      };
      const blockquoteSrc = (match, p1, offset, htmlString) => {
        let replacement = p1;
        if (replacement.match(/twitter/g)) {
          replacement = replacement.replace(/<script([^>]*)src=(''|'')/g, '<script$1src=https://platform.twitter.com/widgets.js');
        }
        return '<iframe>' + replacement + '</iframe>';
      };
      const getBody = (htmlString) => {
        const starsWithDiv = /^(<div>)/g;
        const modifiedhtmlString = starsWithDiv.test(htmlString) ? htmlString : '<div>' + htmlString + '</div>';
        const linebreaks = /\r?\n|\r/g,
          bold = /<b>((?:<br>)*)([^<]+?)((?:<br>)*)<\/b>/g,
          italic = /<i>((?:<br>)*)([^<]+?)((?:<br>)*)<\/i>/g,
          ignoreBIOpen = /<((b)|(i))((\s+[^>]*?)|)>/g,
          ignoreBIClose = /<\/((b)|(i))>/g,
          comments = /<!--[\s\S]*?-->/g,
          ignoredTagsOpen = /<((span)|(font)|(u)|(o:p))((\s+[^>]*?)|)>/g,
          ignoredTagsClose = /<\/((span)|(font)|(u)|(o:p))>/g,
          twttrScript = '<script>twttr.widgets.load();<\/script>',
          divOpen = /(<div)|(<h([1-6]))|(<span)/g,
          divClose = /(<\/div>)|(<\/h([1-6])>)|(<\/span>)/g,
          imgLink = /(<a[^>]*?>)(<img[^>]*?>)(<\/a>)/g,
          imgTag = /(<img[^>]*?>)/g,
          iframeTag = /(<iframe[\s\S^>]*?><\/iframe>)/g,
          instagramTag = /(<iframe(?:(?!iframe).)*instagram(?:(?!iframe).)*height='(\d+)')((?:(?!iframe).)*><\/iframe>)/g,
          pTag = /(<p[^>]*?>(?:(?!(?:<p|<\/p>)).)*<\/p>)/g,
          emptyImgTag = /<img([^>]*?)src=''([^>]*?)>/g,
          emptyPTag = /<p[^>]*?>(<br>)*(<script([^>]*?)><\/script>)*(\s)*<\/p>/g,
          blockquoteTag = /(<blockquote(?:(?!blockquote).)*<\/blockquote>\s*(?:<script([^>]*?)><\/script>)*)/g,
          multipleBr = /(\s*<br>\s*)+/g;
        return modifiedhtmlString
          .replace(linebreaks, ' ')
          .replace(comments, '')
          .replace(ignoredTagsOpen, '')
          .replace(ignoredTagsClose, '')
          .replace(twttrScript, '')
          .replace(divOpen, '<p')
          .replace(divClose, '</p>')
          .replace(emptyImgTag, '')
          .replace(blockquoteTag, blockquoteSrc)
          .replace(imgLink, '$1link$3$2')
          .replace(imgTag, moveTagToRoot)
          .replace(iframeTag, moveTagToRoot)
          .replace(instagramTag, '$1 width="$2"$3')
          .replace(bold, '$1<strong>$2</strong>$3')
          .replace(italic, '$1<em>$2</em>$3')
          .replace(ignoreBIOpen, '')
          .replace(ignoreBIClose, '');
      };
      const styleAttribute = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)style=".*?"/g,
        styleDisabledAttribute = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)style_disabled=".*?"/g,
        target = /target=".*?"/g,
        allowFullscreen = /allowfullscreen=".*?"/g,
        frameBorder = /frameborder=".*?"/g,
        allowTag = /allow=".*?"/g,
        allowtransparencyTag = /allowtransparency=".*?"/g,
        imgEmptySrc = /src=(''|"")/g,
        imgTag = /<img([^>]*?>)/g,
        border = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)border='.*?'/g,
        shape = /(<(?:img|p|span|a|em|iframe)\s+[^>]*?)v:shapes='.*?'/g,
        ampOpenTag = /<(iframe|video)([^>]*?>)/g,
        ampCloseTag = /<\/(iframe|video)>/g,
        youtubeEmbed = /<iframe[^>]*?src='(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:(?:youtube\.com|youtu.be))(?:\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(?:\S+)?'[^>]*?><\/iframe>/g,
        youtubeEmbedNew = /<iframe(?:(?!iframe).)*youtube\.com\/embed\/([^\/]*)\"(?:(?!iframe).)*<\/iframe>/g,
        twitterEmbed = /<iframe>(<blockquote)((?:(?!blockquote).)*(?:https?:\/\/twitter\.com\/(?:#!\/)?(?:\w+)\/status(?:es)?\/(\d+))(?:(?!blockquote).)*<\/blockquote>)\s*(?:<script[^>]*?><\/script>)*<\/iframe>/g,
        instagramEmbed = /<iframe(?:(?!iframe).)*instagram\.com\/p\/([^\/]*)\/(?:(?!iframe).)*<\/iframe>/g,
        facebookEmbed = /<iframe[^>]*fbEmbed\?videoId=(\d+)[^>]*><\/iframe>/,
        script = /<script[^>]*>(?:(?!script).)*<\/script>/g;
      const modifiedhtmlString = getBody(article.body)
        .replace(youtubeEmbed, `<amp-youtube width='480' data-videoid='$1' layout='responsive' height='270'></amp-youtube>`)
        .replace(youtubeEmbedNew, `<amp-youtube width="480" height="270" layout="responsive" data-videoid="$1"></amp-youtube>`)
        .replace(twitterEmbed, `<amp-twitter width=486 height=657 layout='responsive' data-tweetid='$3'>$1 placeholder$2</amp-twitter>`)
        .replace(instagramEmbed, `<amp-instagram width='400' data-shortcode='$1' height='400' layout='responsive'></amp-instagram>`)
        .replace(facebookEmbed, `<amp-facebook width=552 height=574 layout="responsive" data-embed-as="vide" data-href='https://www.facebook.com/facebook/videos/$1/'></amp-facebook>`)
        .replace(styleAttribute, '$1')
        .replace(styleDisabledAttribute, '$1')
        .replace(border, '$1')
        .replace(shape, '$1')
        .replace(imgEmptySrc, `src='https://via.placeholder.com/1' width='1' height='1'`)
        .replace(imgTag, `<amp-img class='contain' height="332px" width="500px"  layout='responsive' $1<\/amp-img>`)
        .replace(ampOpenTag, '<amp-$1  width="480" $2')
        .replace(ampCloseTag, '<\/amp-$1>')
        .replace(script, '')
        .replace(target, '')
        .replace(allowFullscreen, '')
        .replace(frameBorder, '')
        .replace(allowTag, '')
        .replace(allowtransparencyTag, '');
        article.body = modifiedhtmlString;

      article.title = article.title.replace(/"/g, '&quot;').replace(/'/, '&apos;');
      article.shortBody = article.shortBody.replace(/"/g, '&quot;').replace(/'/, '&apos;');
      let mKeyword = (article.metaKeywords) ? article.metaKeywords : '';
      let relatedHtml = (article.related) ? article.related : [];
      relatedHtml = article.related.map(related => {
        const stripURL = '/' + related.shareURL.split('/').slice(3).join('/');
        return `<li><a href='${stripURL}'>
            <amp-img src="${related.thumbnail}" class='contain' width='80' height='80'></amp-img>
            <p>${related.title}</p></a></li>`;
      }).join('');
      let mediaHtml = (article.media && article.media.length > 0) ? article.media : [];
        mediaHtml = article.media.map(item => {
        return `
        <amp-img src="${item.thumbnail}" class="contain" width="1280" height="768" layout="responsive" alt="${item.title}"></amp-img>
        <p><strong>${item.title}</strong><br></p>`;
        }).join('');
      _html = `
      <!doctype html>
      <html ⚡>
      <head>
        <meta charset='utf-8'>
        <script async src='https://cdn.ampproject.org/v0.js'></script>
        <title>${article.title}</title>
        <meta name="description" content="${article.shortBody}">
        <script type='application/ld+json'>
        [{
          "@context": "http://schema.org",
           "@type": "NewsArticle",
          "mainEntityOfPage":{
            "@type": "WebPage",
            "@id": "${article.shareURL}"
          },
          "headline": "${article.title}",
          "articleSection":"${article.mainSection.parentSection}",
          "image":{
          "@type": "ImageObject",
          "url": "${article.thumbnail}",
          "height": 800,
          "width": 500
          },
          "datePublished": "${article.publishedAt}",
          "dateModified": "${article.publishedAt}",
          "author":{
              "type":"Person",
              "name":"${article.authorName}"},
          "publisher":{
              "@type": "Organization",
              "name": "Spotboye",
              "url":"https://www.spotboye.com/",
              "logo": {
                  "@type": "ImageObject",
                  "url": "https://www.spotboye.com/assets/images/logo.png",
                  "width": 180,
                  "height": 40
              }
          },
          "description":"${article.shortBody}",
          "url": "${article.shareURL}"
        }]
        </script>
        <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": "${article.title}",
            "description": "${article.shortBody}",
            "keywords":"${mKeyword}",
            "url" : "${article.shareURL}"
        }
        </script>
        <script type="application/ld+json">
          {
          "@context": "http://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement":
          [
          {
          "@type": "ListItem",
          "position": 1,
          "item":
          {
          "@id": "https://www.spotboye.com/",
          "name": "HOME"
          }
          },
          {
          "@type": "ListItem",
          "position": 2,
          "item":{
          "@id": "https://www.spotboye.com/${article.mainSection.parentSlug}/",
          "name": "${article.mainSection.parentSection}"
          }
          }
          ,{
          "@type": "ListItem",
          "position": 3,
          "item":{
          "@id": "https://www.spotboye.com/${article.mainSection.parentSlug}/${article.mainSection.slug}/",
          "name": "${article.mainSection.title}"
          }
          }
          ]}
        </script>
        <link rel='canonical' href='${article.shareURL}' />
        <meta property="og:title" content="${article.title}">
        <meta property="og:type" content="article" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:description" content="${article.shortBody}" />
        <meta property="og:image" content="${article.thumbnail}" />
        <meta property="og:image:type" content="image/jpeg" />
        <meta property="og:image:width" content="800" />
        <meta property="og:image:height" content="500" />
        <meta property="og:url" content="${article.shareURL}" />

        <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet'>
        <meta name='viewport' content='width=device-width,minimum-scale=1,initial-scale=1'>
        <link rel='manifest' href='/manifest.json'>
        <!-- See https://goo.gl/qRE0vM -->
        <!-- Add to homescreen for Chrome on Android. Fallback for manifest.json -->
        <meta name='mobile-web-app-capable' content='yes'>
        <meta name='application-name' content='SpotboyE App'>
        <!-- Add to homescreen for Safari on iOS -->
        <meta name='apple-mobile-web-app-capable' content='yes'>
        <meta name='apple-mobile-web-app-status-bar-style' content='black-translucent'>
        <meta name='apple-mobile-web-app-title' content='SpotboyE App'>
        <!-- Homescreen icons -->
        <link rel='apple-touch-icon' href='/assets/images/manifest/icon-48x48.png'>
        <link rel='apple-touch-icon' sizes='72x72' href='/assets/images/manifest/icon-72x72.png'>
        <link rel='apple-touch-icon' sizes='96x96' href='/assets/images/manifest/icon-96x96.png'>
        <link rel='apple-touch-icon' sizes='144x144' href='/assets/images/manifest/icon-144x144.png'>
        <link rel='apple-touch-icon' sizes='192x192' href='/assets/images/manifest/icon-192x192.png'>
        <!-- Tile icon for Windows 8 (144x144 + tile color) -->
        <meta name='msapplication-TileImage' content='/assets/images/manifest/icon-144x144.png'>
        <meta name='msapplication-TileColor' content='#3f51b5'>
        <meta name='msapplication-tap-highlight' content='no'>
        <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,700&display=swap" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet">
        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
        <noscript><style amp-boilerplate>
        body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <script custom-element='amp-sidebar' src='https://cdn.ampproject.org/v0/amp-sidebar-0.1.js' async=''></script>
        <script custom-element='amp-instagram' src='https://cdn.ampproject.org/v0/amp-instagram-0.1.js' async=''></script>
        <script custom-element='amp-social-share' src='https://cdn.ampproject.org/v0/amp-social-share-0.1.js' async=''></script>
        <script async custom-element='amp-twitter' src='https://cdn.ampproject.org/v0/amp-twitter-0.1.js'></script>
        <script async custom-element='amp-facebook' src='https://cdn.ampproject.org/v0/amp-facebook-0.1.js'></script>
        <script async custom-element='amp-ad' src='https://cdn.ampproject.org/v0/amp-ad-0.1.js'></script>
        <script async custom-element='amp-youtube' src='https://cdn.ampproject.org/v0/amp-youtube-0.1.js'></script>
        <script async custom-element='amp-iframe' src='https://cdn.ampproject.org/v0/amp-iframe-0.1.js'></script>
        <script async custom-element='amp-fx-flying-carpet' src='https://cdn.ampproject.org/v0/amp-fx-flying-carpet-0.1.js'></script>
        <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
        <style amp-custom>
          .header {
            min-height: 42px;
            max-height: 42px;
            background-color: #000;
            padding: 8px;
            border-bottom: 2px solid yellow;
          }
          body {
            background-color: white;
            /*font-family: 'Raleway', sans-serif;*/
            font-family: 'Oswald', sans-serif;
            color:#333;
          }
          .ampstart-headerbar {
          background-color: #000;
          color: #fff;
          z-index: 999;
          box-shadow: 0 0 5px 2px rgba(0,0,0,.1);
          width: 100%;
          top: 0;
          }
          .ampstart-navbar-trigger {
          line-height: 3.5rem;
          font-size: 2rem;
          }
          .ampstart-sidebar {
          background-color: #fff;
          color: #000;
          min-width: 300px;
          width: 300px;
          }
          figure{
            margin: 0;
          }
          amp-ad{
            margin-top: 56px;
            display: block;
          }
          .flex {
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          }
          .fixed{
            position: fixed;
          }
          .pr2 {
          padding-right: 1rem;
          }
          .pr4 {
          padding-right: 2rem;
          }
          .pl2 {
          padding-left: 1rem;
          }
          .px3 {
          padding-left: 1rem;
          padding-right: 2rem;
          }
          .ampstart-sidebar-header {
          line-height: 3.5rem;
          min-height: 3.5rem;
          }
          amp-accordion>section>:nth-child(n) {
          background: transparent;
          border: 0;
          }
          .ampstart-dropdown>section>header:after {
          display: inline-block;
          content: '+';
          padding: 0 0 0 1.5rem;
          color: #000;
      }
      .ampstart-sidebar .ampstart-dropdown-item,
      .ampstart-sidebar .ampstart-dropdown header,
      .ampstart-sidebar .ampstart-faq-item,
      .ampstart-sidebar .ampstart-nav-item, .ampstart-sidebar .ampstart-social-follow {
          margin: 0 0 1rem;
      }
      .ampstart-faq-item a{
        color: #4d4d4d;
      }
      .ampstart-nav-dropdown .ampstart-dropdown-item {
          color: #003f93;
          font-size: 12px;
      }
          .justify-start {
          -webkit-box-pack: start;
          -ms-flex-pack: start;
          justify-content: flex-start;
          }
          .items-center {
              -webkit-box-align: center;
              -ms-flex-align: center;
              align-items: center;
          }
          .flex {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
          }
          .list-reset {
          list-style: none;
          padding-left: 0;
          }
          .caps {
          text-transform: uppercase;
          letter-spacing: .2em;
          }
          .ampstart-sidebar .ampstart-nav-dropdown {
          margin: 0;
          }
          .ampstart-nav-dropdown {
          min-width: 200px;
          }
          .relative {
          position: relative;
          }
          .ampstart-dropdown {
          min-width: 200px;
          }
          .article_header {
            margin-bottom:8px;
          }
          .article_header>.title {
            padding: 8px;
            font-size: 21px;
          font-weight: 500;
          line-height: 27px;
          }
          .article_content{
            padding:8px;
            margin-top:20px;
            font-size: 15px;
            color: #727272;
          }
          .article_header p{
            line-height: inherit;
          font-size: 15px;
          color: #727272;
          margin-left: 8px;
          }
          .article_content>.article_body p{
            line-height: inherit;
            font-size: 1rem;
            color: #727272;
            font-family: 'Oswald', sans-serif;
            font-weight: initial;
          }
          .article_header>h3{
            padding:8px;
            margin:0;
          }
          .article_related>ul{
            list-style:none;
            padding:0;
          }
          .article_related>ul li{
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            margin-bottom: 10px;
            border-bottom: 1px solid #ccc;
            padding-bottom: 10px;
          }
          .article_related{
            background: #f5f5f5;
          margin: 0;
          margin-left: -8px;
          margin-right: -8px;
          padding: 8px;
          }
          .article_related amp-img{
            float: left;
          }
          amp-img.contain img {
          object-fit: contain;
        }
          .article_related>ul li p{
            -webkit-box-flex: 1;
          -ms-flex: 1;
          flex: 1;
            margin: 0px;
          margin-left: 10px;
          float: left;
          width: 72%;
          }
          .article_related>ul li a{
            color: #4d4d4d;
          }
          .author_box_inside{
          overflow: hidden;
          margin-top: 20px;
          margin-bottom: 20px;
          color: #7f7f7f;
          width: 100%;
          }
          .author_box_inside amp-img{height:100px; width:100px; float:left;}
          .author_box_inside p{color: #a8a8a8; font-size: 14px; margin-left: 8px; display: inline-block;}
          .author_box_inside p a{color: #a8a8a8;}
          .author_box_inside b{font-weight: normal;margin-left: 8px;}
          .author_box_inside span
          {display: inline-block; text-align: center;height: 23px;font-size: 14px;color: #a8a8a8;font-weight: 400;}
          amp-social-share{
          width: 34px ;
          display: inline-block;
          text-align: center;
          font-size: 19px;
          height: 34px ;
          line-height: 34px;
          color: #fff;
          margin-left: 7px;
          }
          .logo{
            margin: 0 auto;margin-left: 52px;
          }
          .ampad{
            width: 100%;
            background: #f5f5f5;
            text-align: center;
          }
          ul.breadcrumb {
            list-style: none;
            width: 100%;
            margin: 10px 0px;
            padding: 0 10px;
          }

          ul.breadcrumb li {
            display: inline;
            font-size: 1.1rem;
            color: #ff005b;
          }
          ul.breadcrumb li a {
            color: #ff005b;
          }

          ul.breadcrumb li span {
            color: #ff005b;
            margin: 0px 4px;

          }


          a, a:visited {opacity: 1;
            text-decoration:none;
            transition:color .25s,background .25s,opacity .25s;
        }
        .amp-container{max-width:1200px; width:100%; margin:0 auto;}
        #hidingScrollBar{overflow:hidden;position:relative;}
        .hideScrollBar{width: 100%; overflow:auto; margin-right:14px; padding-right:10px;}
        .amp-header{ width:100%; position:fixed; background:#fff; top:56px; z-index:9999 }
        .amp-header .hideScrollBar ul li+li{border-left:1px solid #cecece;}
        .amp-header .hideScrollBar ul{width:100%; display:flex; white-space:nowrap; list-style:none; text-transform:uppercase; padding: 0px; margin:0px; border-bottom: 1px solid #cecece;}
        .amp-header .hideScrollBar ul li a{color:#333; font-size:14px; display:block; padding:10px 20px;}
        .amp-header .hideScrollBar ul li a:hover{color:#000;}
        /*Footer css*/
        footer{background:#111; padding-top:40px; color:#888;}
        .footer-logo{text-align:center; padding-bottom:24px;}
        .media_icon ul{text-align:center; list-style:none; padding-bottom:30px;}
        .media_icon ul li{display:inline-block; vertical-align:top; margin:0px 2px;}
        .media_icon ul li a{width:40px; height:40px; border-radius:50%; background:#333; color:#fff;}
        .media_icon ul li a.fab{font-size:20px; line-height:40px;}
        .media_icon ul li a.fa-facebook-f:hover{background:#3b5997;}
        .media_icon ul li a.fa-twitter:hover{background:#00aced;}
        .media_icon ul li a.fa-pinterest-p:hover{background:#cb2027;}
        .media_icon ul li a.fa-google-plus-g:hover{background:#cd3627;}
        .media_icon ul li a.fa-youtube:hover{background:red;}
        .footer_list ul{text-align:center; list-style:none;}
        .footer_list ul li{display:inline-block; vertical-align:top; margin:0px 10px  10px 10px;}
        .footer_list ul li a{color:#888; font-size:.9rem; font-weight:400; letter-spacing:1px; text-transform:uppercase; padding:10px 8px;}
        .footer_list ul li a:hover{color:#fff;}
        .footer_bg{background:#000; margin-top:30px; padding:20px 0px;}
        .footer_copy{width:49%; display:inline-block; vertical-align:top;}
        .footer_copy p{color:#777; font-size:.9rem; font-weight:400;line-height:1.4;}
        .footer_copy ul{text-align:right;}
        .footer_copy ul li{list-style:none; display:inline-block; vertical-align:top;}
        .footer_copy ul li a{color:#888; font-size:.9rem; font-weight:400; border-left:1px solid #777; padding:0px 4px 0px 8px;}
        .footer_copy ul li a:hover{color:#fff;}
        .footer_copy ul li:first-child a{border-left:0px;}
        .sliderAd{ margin-top:50px;}
        @media screen and (max-width:768px){
          .footer_copy{width:100%; display:block; text-align:center;}
          .footer_copy ul{text-align:center; margin:20px 0px 0px 0px; padding:0px;}
        }
        </style>
             <script type='application/ld+json'>
                      {
                          "@context": "http://schema.org",
                          "@type": "Organization",
                          "url": "https://www.spotboye.com",
                          "sameAs": [
                              "https://www.facebook.com/Spotboye/",
                              "https://twitter.com/@spotboye",
                              "https://www.instagram.com/spotboye/",
                              "http://www.youtube.com/user/9xetheshow"
                          ],
                          "contactPoint": [{
                              "@type": "ContactPoint",
                              "telephone": "+91-22-6601 9999",
                              "contactType": "customer service"
                          }]
                      }
                  </script>
                  <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>


      </head>
<body>
<!-- Start Navbar -->
<header class='ampstart-headerbar fixed flex justify-start items-center top-0 left-0 right-0 pl2 pr4'>
  <div role='button' on='tap:header-sidebar.toggle' tabindex='0' class='ampstart-navbar-trigger  pr2'>☰</div>
  <a href='https://www.spotboye.com'>
  <amp-img class='logo' src='https://www.spotboye.com/assets/images/flat_logo.png' height='26' width='150'></amp-img>
</a>
</header>
<div class="amp-header">
<div class="amp-container" id="hidingScrollBar">
<div class="hideScrollBar">
  <ul>
    <li><a href="https://www.spotboye.com/bollywood">Bollywood</a></li>
    <li><a href="https://www.spotboye.com/television">Television</a></li>
    <li><a href="https://www.spotboye.com/music">Music</a></li>
    <li><a href="https://www.spotboye.com/fashion">Fashion</a></li>
    <li><a href="https://www.spotboye.com/just-binge">Just Binge</a></li>
    <li><a href="https://www.spotboye.com/vod">Videos</a></li>
    <li><a href="https://www.spotboye.com/photos">Photos</a></li>
    <li><a href="https://www.spotboye.com/trending">Trending</a></li>
    <li><a href="https://www.spotboye.com/regional">Regional</a></li>
  </ul>
</div>
</div>
</div>



<!-- Start Sidebar -->
<amp-sidebar id='header-sidebar' class='ampstart-sidebar px3 ' layout='nodisplay'>
<div class='flex justify-start items-center ampstart-sidebar-header'>
  <div role='button' on='tap:header-sidebar.toggle' tabindex='0' class='ampstart-navbar-trigger items-start'>✕</div>
</div>
<nav class='ampstart-sidebar-nav ampstart-nav'>
  <ul class='list-reset m0 p0 caps h5'>
        <li class='ampstart-nav-item ampstart-nav-dropdown relative'>

<!-- Start Dropdown-inline
<amp-accordion layout='container' disable-session-states='' class='ampstart-dropdown'>-->

<section>
${menusHtml}
</section>

<!-- </amp-accordion> -->
<!-- End Dropdown-inline -->
        </li>
        <li class='ampstart-nav-item ampstart-nav-dropdown relative'>

<!-- Start Dropdown-inline -->
<!-- <amp-accordion layout='container' disable-session-states='' class='ampstart-dropdown'>
 <% menus.forEach(function(menu){ }
<section>
   <header>{menu.title}</header>
  <ul class='ampstart-dropdown-items list-reset m0 p0'>
  <% for(var i=0;i<2;i++){ }
      <li class='ampstart-dropdown-item'><a href='#' class='text-decoration-none'>{menu.subMenu}</a></li>
        <% }; }
  </ul>
</section>
  <%})}
</amp-accordion> -->
<!-- End Dropdown-inline -->
        </li>
  </ul>
</nav>
<ul class='ampstart-social-follow list-reset flex justify-around items-center flex-wrap m0 mb4'>
  <li class='mr2'>
      <a href='https://twitter.com/@spotboye' class='inline-block'>
      <svg xmlns='http://www.w3.org/2000/svg' width='24' height='22.2' viewbox='0 0 53 49'>
      <title>Twitter</title>
      </path></svg></a>
  </li>
  <li class='mr2'>
      <a href='https://www.facebook.com/Spotboye/' class='inline-block'>
      <svg xmlns='http://www.w3.org/2000/svg' width='24' height='23.6' viewbox='0 0 56 55'>
      <title>Facebook</title>
      </svg></a>
  </li>
  <li class='mr2'>
      <a href='https://www.instagram.com/spotboye/' class='inline-block'>
      <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewbox='0 0 54 54'>
      <title>instagram</title>
      </svg></a>
  </li>

</ul>

  <ul class='ampstart-sidebar-faq list-reset m0'>
    <li class='ampstart-faq-item'><a href='https://www.spotboye.com/terms' class='text-decoration-none'>Terms &amp; Conditions</a></li>
    <li class='ampstart-faq-item'><a href='https://www.spotboye.com/disclaimer' class='text-decoration-none'>Disclaimer</a></li>
    <li class='ampstart-faq-item'><a href='https://www.spotboye.com/privacypolicy' class='text-decoration-none'>Privacy Policy</a></li>
    <li class='ampstart-faq-item'><a href='https://www.spotboye.com/contactus' class='text-decoration-none'>Contact</a></li>
    <li class='ampstart-faq-item'><a href='https://www.spotboye.com/aboutus' class='text-decoration-none'>About</a></li>
    <li class='ampstart-faq-item'><a href='https://www.spotboye.com/sitemap' class='text-decoration-none'>Sitemap</a></li>
  </ul>


</amp-sidebar>
<!-- <div class='header'>
  <amp-img src='https://www.spotboye.com/assets/images/flat_logo.png' height='40' width='180'></amp-img>
</div> -->
<div class='ampad sliderAd'>
<amp-ad width=320 height=50 type='doubleclick' data-slot='/97172535/SECTION3_728X90'>
<div placeholder></div>
<div fallback></div>
</amp-ad>
</div>

  <div class="bred-section">
  <ul class="breadcrumb">
  <li><a href="/">Home</a><span>/</span></li>
  <li><a href="https://www.spotboye.com/${article.mainSection.parentSlug}/">${article.mainSection.parentSection}</a><span>/</span></li>
  <li><a href="https://www.spotboye.com/${article.mainSection.parentSlug}/${article.mainSection.slug}/">${article.mainSection.title}</a></li>
  </ul>
  </div>
  <div class='article_header'>
  <h1 class='title'>${article.title}</h1>
  <p>${article.shortBody}</p>

  <div class='ampstart-social-box mb4'>
    <amp-social-share type='twitter'></amp-social-share>
    <amp-social-share type='facebook' data-param-text="${encodeURI(article.title)}" data-param-href="${article.shareURL}"
    data-param-app_id='1823404977913088'></amp-social-share>
    <amp-social-share type='whatsapp'></amp-social-share>
  </div>

  <amp-img src="${article.thumbnail}" width='1280' height='768' layout='responsive'></amp-img>

  <!-- <h3 class=''>${new Date(article.publishedAt).toTimeString()} -- ${article.publishedAt} -- ${dtNew.toLocaleTimeString('en-US')} --india-- ${indiaTimeNew.toLocaleString()} --timeonly-- ${dTimearrNew[0]} ---Usa-- ${usaTimeNew.toLocaleString()} ---asia--- ${asiaTimeNew.toLocaleString()}</h3> -->
  </div>

  <div class='author_box_inside'>
    <div class='ampad'>
      <amp-ad width=320 height=100 type='doubleclick' data-slot='/97172535/Amp_320x100'>
        <div placeholder></div>
        <div fallback></div>
      </amp-ad>
    </div>
    <div>
      <amp-img src="${article.author.profilePic}" width='100' height='100' layout='responsive'></amp-img>
    </div>
    <p><a href="https://www.spotboye.com/author/${article.author.slug}/" title="Posts by ${article.authorName}" rel="author">
      ${article.authorName}</a></p>
    <span><b>${new Date(article.publishedAt).toDateString()} ${dTimearrNew[0]}</b></span>
    <span><b>${article.views}</b> views</span>
  </div>

<div class='article_content'>
  <div class='article_body'>${article.body}</div>
  <div>${mediaHtml}</div>
<div class='ampad'>
<amp-ad width=300 height=250 type='doubleclick' data-slot='/97172535/MOBILE_ARICLE_2_320X250'>
<div placeholder></div>
<div fallback></div>
</amp-ad>
</div>
  <!-- <div class='amp-flying-carpet-text-border'>Advertising</div>
    <amp-fx-flying-carpet height='300px'>
      <amp-ad width='300'
        height='600'
        type='adsense'
        data-ad-client='ca-pub-2223758409184752'
        data-ad-slot='8768649543'>
      </amp-ad>
    </amp-fx-flying-carpet>
    <div class='amp-flying-carpet-text-border'>Advertising</div> -->
    <amp-ad data-amp-slot-index='2' data-loading-strategy='1.25' data-clmb_divid='c' data-clmb_section='0'
    data-clmb_position='1' data-clmb_slot='231691' layout='responsive' type='colombia' height='267' width='360'
    heights='(min-width:1907px) 100%, (min-width:1200px) 100%,
    (min-width:780px) 100%, (min-width:420px) 105%, (min-width:380px) 370%, (min-width:340px) 380%, 400%'></amp-ad>
    <div class='article_related'>
    <h3>RELATED NEWS</h3>
    <ul>
 ${relatedHtml}
</ul>

    </div>
<div class='ampad'>
    <amp-ad width=300 height=250 type='doubleclick' data-slot='/97172535/MOBILE_ARICLE_2_320X250'>
      <div placeholder></div>
      <div fallback></div>
    </amp-ad>
</div>
  </div>

  <footer>
	<div class="amp-container">
		<div class="footer-logo"><a href="https://www.spotboye.com/"><amp-img src="https://www.spotboye.com/assets/images/flat_logo.png" width="184" height="31" alt="AMP"></a></div>
		<div class="media_icon">
			<ul>
				<li><a href="https://www.facebook.com/Spotboye/" class="fab fa-facebook-f"></a></li>
				<li><a href="https://twitter.com/spotboye" class="fab fa-twitter"></a></li>
				<li><a href="https://www.pinterest.com/SpotboyE/" class="fab fa-pinterest-p"></a></li>
				<li><a href="https://accounts.google.com/signin/v2/sl/pwd?passive=1209600&osid=1&continue=https%3A%2F%2Fplus.google.com%2F%2Bspotboye%2Fposts&followup=https%3A%2F%2Fplus.google.com%2F%2Bspotboye%2Fposts&flowName=GlifWebSignIn&flowEntry=ServiceLogin" class="fab fa-google-plus-g"></a></li>
				<li><a href="https://www.youtube.com/c/spotboye" class="fab fa-youtube"></a></li>
			</ul>
		</div>
		<div class="footer_list">
			<ul>
				<li><a href="https://www.spotboye.com/bollywood">Bollywood</a></li>
				<li><a href="https://www.spotboye.com/television">Television</a></li>
				<li><a href="https://www.spotboye.com/music">Music</a></li>
				<li><a href="https://www.spotboye.com/fashion">Fashion</a></li>
				<li><a href="https://www.spotboye.com/just-binge">Just Binge</a></li>
				<li><a href="https://www.spotboye.com/vod">Videos</a></li>
				<li><a href="https://www.spotboye.com/photos">Photos</a></li>
				<li><a href="https://www.spotboye.com/trending">Trending</a></li>
				<li><a href="https://www.spotboye.com/regional">Regional</a></li>
				<li><a href="https://www.spotboye.com/celebrity">Celebrity</a></li>
				<li><a href="https://www.spotboye.com/author">Our Authors</a></li>
			</ul>
		</div>
	</div>
	<div class="footer_bg">
		<div class="amp-container">
			<div class="footer_copy">
				<p>&copy; 2019 9X Media Pvt. Ltd. All Rights Reserved.</p>
			</div>
			<div class="footer_copy">
				<ul>
					<li><a href="https://www.spotboye.com/footers/terms-conditions">Terms & Conditions</a></li>
					<li><a href="https://www.spotboye.com/footers/disclaimer">Disclaimer</a></li>
					<li><a href="https://www.spotboye.com/footers/privacy-policy">Privacy Policy</a></li>
					<li><a href="https://www.spotboye.com/footers/contact-us">Contact Us</a></li>
					<li><a href="https://www.spotboye.com/footers/about-us">About Us</a></li>
					<li><a href="https://www.spotboye.com/footers/sitemap">Sitemap</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>

<amp-analytics type="googleanalytics" id="analytics1">
  <script type="application/json">
    {
      "vars": {
        "account": "UA-53522784-1"
      },
      "triggers": {
        "trackPageview": {
          "on": "visible",
          "request": "pageview"
        }
      }
    }
  </script>
</amp-analytics>
<amp-analytics type='comscore'>
  <script type='application/json'>
{
"vars": {
 "c2": "19264324"
},
"extraUrlParams": {
 "comscorekw": "amp"
}
}
</script>
</amp-analytics>
</body>
      </html>
      `;
    } else {
      _html = `
      <html>
      <head></head>
      <body>
      <h2>Article doesn't exist</h2>
      </body>
      </html>
      `;
    }
    res.send(_html);
  };

  if (req.params.id) {
    const options = {
      host: 'apiv3.spotboye.com',
      port: 80,
      path: `/web/article/${req.params.id}`,
      method: 'GET'
    };

    const request = http.request(options, function (result: any) {
      let _d = '';
      // res.setEncoding('utf8');
      result.on('data', function (chunk) {
        _d += chunk;
      });
      result.on('end', function () {
        render(_d);
      });
    });
    request.setHeader('User-Agent', (req.header('User-Agent')) ? req.header('User-Agent') : 'Google Chrome');
    request.on('error', function (e) {
      res.send('problem with request: ' + e.message);
    });
    request.end();

    // let _html = '<h1>Hello</h1>';
    // _html += '<h2>item</h2>';
    // res.send(_html);
    // res.render('amp', { req, res }, (err, html) => {
    //   if (html) {
    //     res.send(html);
    //   } else {
    //     console.error(err);
    //     res.send(err);
    //   }
    // });
  } else {
    res.send('Invalid Article');
  }
});
/** AMP RELATED ENDS */



app.get('/healthcheck', (req, res) => {
  res.send('OK');
});
app.get('/health', (req, res) => {
  res.send('OK');
});
// app.get('/arr', (req, res) => {
//   res.set('content-type', 'application/xml');
//   got('apiv3.spotboye.com/web/xml?for=/googlesitemap.xml', { xml: true }).then(response => {
//     res.send(response.body);
//   }).catch(error => {
//     console.log(error);
//     res.send(error);
//   });
// });
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', './dist/browser');


app.get('/*', (req, res) => {
  res.render('index', { req, res }, (err, html) => {
    if (html) {
      res.send(html);
    } else {
      console.error(err);
      res.send(err);
    }
  });
});

http.createServer(app).listen(3000,()=>{
  console.log('app is listing on 3000')
})
